<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190125095702 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE invitation_membership_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE invitation_membership (id INT NOT NULL, id_account_id INT DEFAULT NULL, id_user_id INT DEFAULT NULL, confirm BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1CCEFFC83EE1DF6D ON invitation_membership (id_account_id)');
        $this->addSql('CREATE INDEX IDX_1CCEFFC879F37AE5 ON invitation_membership (id_user_id)');
        $this->addSql('ALTER TABLE invitation_membership ADD CONSTRAINT FK_1CCEFFC83EE1DF6D FOREIGN KEY (id_account_id) REFERENCES account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE invitation_membership ADD CONSTRAINT FK_1CCEFFC879F37AE5 FOREIGN KEY (id_user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" ALTER first_name TYPE VARCHAR(50)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE invitation_membership_id_seq CASCADE');
        $this->addSql('DROP TABLE invitation_membership');
        $this->addSql('ALTER TABLE "user" ALTER first_name TYPE VARCHAR(255)');
    }
}
