<?php

namespace App\Repository;

use App\Entity\InvitationMembership;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InvitationMembership|null find($id, $lockMode = null, $lockVersion = null)
 * @method InvitationMembership|null findOneBy(array $criteria, array $orderBy = null)
 * @method InvitationMembership[]    findAll()
 * @method InvitationMembership[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InvitationMembershipRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InvitationMembership::class);
    }

    // /**
    //  * @return InvitationMembership[] Returns an array of InvitationMembership objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InvitationMembership
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
