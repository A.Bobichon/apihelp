<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 09/10/18
 * Time: 14:50
 */

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Account;
use App\Entity\Membership;
use App\Entity\Story;
use App\Entity\Library;
use App\Entity\User;
use App\Exception\NotFoundException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Runner\Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

final class DefaultEntityParameters implements EventSubscriberInterface {
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    private $authorizationChecker;
    private $entityManager;

    public function __construct(TokenStorageInterface $tokenStorage, AuthorizationCheckerInterface $checker, EntityManagerInterface $entityManager) {
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $checker;
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents() {
        return [
            KernelEvents::VIEW => ['defaultEntityParameters', EventPriorities::PRE_VALIDATE],
        ];
    }

    /**
     * Add defaults parameters to entity when POST PUT DELETE
     * @param GetResponseForControllerResultEvent $event
     */
    public function defaultEntityParameters(GetResponseForControllerResultEvent $event) {
        $method = $event->getRequest()->getMethod();
        switch ($method) {
            case Request::METHOD_POST:
                $this->postParameters($event);
                break;
            case Request::METHOD_PUT:
                $this->checkAutorization($event);
                break;
        }
    }

    /**
     * Sub POST parameters
     * @param GetResponseForControllerResultEvent $event
     */
    public function postParameters(GetResponseForControllerResultEvent $event) {
        $this->checkEntityParameters($entity = $event->getControllerResult());
    }

    /**
     * Check entity default parameters
     * @param $entity
     */
    private function checkEntityParameters($entity) {
        $setOwner = "setOwner";

        // Specific entity parameters
        switch (true) {
            case $entity instanceof User:
                // User parameters
                $setOwner = null;
                if ($entity->getType() === null) $entity->setType("user");
                break;
            case $entity instanceof Account:
                // User parameters
                $setOwner = "setAdminUser";
                if ($entity->getCode() === null) {
                    $entity->setCode($this->generateCode());
                }
                break;
            case $entity instanceof Story:
                // Story parameters
                break;
            case $entity instanceof Library:
                // Library parameters
                $entity->setIsTransfered(false);
                break;
            case $entity instanceof Membership:
                // Membership parameters
                $setOwner = "setUser";
                if ($entity->getIsAdmin() === null) {
                    $entity->setIsAdmin(false);
                }
                break;
        }

        // Add updated date
        $entity->setUpdatedAt(new \DateTimeImmutable());

        // Add isActive boolean
        if ($entity->getIsActive() === null) {
            $entity->setIsActive(true);
        }

        // Add owner of entity
        $token = $this->tokenStorage->getToken();
        if ($token && $setOwner !== null) {
            $owner = $token->getUser();
            if ($owner instanceof User) {
                $entity->$setOwner($owner);
            }
        }

        // Usecase of membership + account creation
        if ($entity instanceof Membership && $entity->getAccount() !== null) {
            $this->checkEntityParameters($entity->getAccount());
        }
    }

    /**
     * Check PUT autorization
     * @param GetResponseForControllerResultEvent $event
     * @throws NotFoundException
     */
    public function checkAutorization(GetResponseForControllerResultEvent $event) {
        $entity = $event->getControllerResult();

        // Specific entity parameters
        switch (true) {
            case $entity instanceof Account:
                // Only admin can update account
                $token = $this->tokenStorage->getToken();
                $owner = $token->getUser();

                $membership = $this->entityManager->getRepository(Membership::class)
                    ->findOneBy([
                        'user' => $owner,
                        'account' => $entity,
                    ]);

                if (!$membership->getIsAdmin()) {
                    throw new NotFoundException('Only administrator can update account');
                }
                break;
            case $entity instanceof Membership:
                // Only owner or admin can update membership
                $token = $this->tokenStorage->getToken();
                $owner = $token->getUser();

                if ($entity->getUser() === $owner) {
                    return;
                }

                $membership = $this->entityManager->getRepository(Membership::class)
                    ->findOneBy([
                        'user' => $owner,
                        'account' => $entity->getAccount(),
                    ]);

                if (!$membership->getIsAdmin())
                    throw new NotFoundException('Only owner or administrator can update membership');
                break;
        }
    }

    private function generateCode($length = 7) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }
}