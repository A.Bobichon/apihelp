<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 02/10/18
 * Time: 21:59
 */

namespace App\Controller;

use App\Entity\ComfirmToken;
use App\Entity\User;
use App\Exception\NotFoundException;
use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWSProvider\JWSProviderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use MongoDB\Driver\Exception\ExecutionTimeoutException;
use ApiPlatform\Core\Exception\InvalidArgumentException;
use PHPUnit\Runner\Exception;
use Sendinblue\Mailin;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @Route("/api")
 */
class UserController extends AbstractController
{
    /**
     * @Route(
     *     name="api_users_post",
     *     path="/users",
     *     methods={"POST"},
     *     defaults={
     *         "_api_resource_class"=User::class,
     *         "_api_collection_operation_name"="post"
     *     }
     * )
     */
    public function postAction(User $data, UserPasswordEncoderInterface $encoder): User
    {
        $data->setRole('ROLE_USER_APP'); // Default
        return $this->encodePassword($data, $encoder);
    }

    /**
     * @Route(
     *     name="api_users_put",
     *     path="/users/{id}",
     *     requirements={"id"="\d+"},
     *     methods={"PUT"},
     *     defaults={
     *         "_api_resource_class"=User::class,
     *         "_api_item_operation_name"="put"
     *     }
     * )
     */
    public function putAction(User $data, UserPasswordEncoderInterface $encoder): User
    {
        //return $this->encodePassword($data, $encoder);
        return $data;
    }

    protected function encodePassword(User $data, UserPasswordEncoderInterface $encoder): User
    {
        switch (true) {
            case $data->getPassword() == null:
                throw new InvalidArgumentException("Your password is null");
            case $data->getPassword() == "":
                throw new InvalidArgumentException("Your password is empty");
            case strlen($data->getPassword()) <= 5:
                throw new InvalidArgumentException("Your password must be at least 5 characters long");
            case strlen($data->getPassword()) > 45:
                throw new InvalidArgumentException("Your password cannot be longer than 45 characters");
        }

        $encoded = $encoder->encodePassword($data, $data->getPassword());
        $data->setPassword($encoded);

        return $data;

        // TODO : Update password 200 exepted 400
    }

    /**
     * @Route(
     *     name="api_users_password_change",
     *     path="/users/password/change",
     *     methods={"POST"},
     *     defaults={
     *         "_api_resource_class"=User::class,
     *         "_api_collection_operation_name"="post"
     *     }
     * )
     * Change user password
     *
     */
    public function changePassword(User $data, UserPasswordEncoderInterface $encoder, UserRepository $repository) {
        $user = $repository->findOneBy(array("username" => $data->getUsername()));
        if ($data->getTokenPassword() !== null) {
            if ($data->getTokenPassword() === $user->getTokenPassword()) {
                $encoded = $encoder->encodePassword($data, $data->getNewPassword());
                $user->setPassword($encoded);
                $em = $this->getDoctrine()->getManager();

                $em->persist($user);
                $em->flush();
            } else {
                throw new Exception("the token is wrong");
            }
        } elseif ($data->getPassword() !== null) {
            if ($data->getPassword() === $user->getPassword()) {
                if ($encoder->isPasswordValid($user, $data->getPassword()) && !$encoder->isPasswordValid($user, $data->getNewPassword())) {

                    $encoded = $encoder->encodePassword($data, $data->getNewPassword());
                    $user->setPassword($encoded);
                    $em = $this->getDoctrine()->getManager();

                    $em->persist($user);
                    $em->flush();
                }
            }
        }
    }

        /*
        // Grep user from database
        $user = $repository->findOneBy(array("username" => $data->getUsername()));
        if ($user !== null && $encoder->isPasswordValid($user, $data->getPassword())) {
            if (!$encoder->isPasswordValid($user, $data->getNewPassword())) {
                if ( strlen($data->getNewPassword()) > 5 && strlen($data->getNewPassword()) < 255) {
                    $encoded = $encoder->encodePassword($data, $data->getNewPassword());
                    $user->setPassword($encoded);
                    $em = $this->getDoctrine()->getManager();

                    $em->persist($user);
                    $em->flush();
                } else {
                    throw new NotFoundException("The password format is between 5 and 255");
                }
                throw new NotFoundException($user->getPassword());
            } else {
                throw new NotFoundException("It's the same password");
            }
        } else {
            throw new NotFoundException("the user is not given:" . $user);
        }
    }

    /**
     * @Route(
     *     name="api_users_password_forget",
     *     path="/users/password/forgot",
     *     methods={"POST"},
     *     defaults={
     *         "_api_resource_class"=User::class,
     *         "_api_collection_operation_name"="post"
     *     }
     * )
     * Change user password
     *
     */
    public function forgetPassword(User $data, UserRepository $repository, JWTTokenManagerInterface $JWTManager)
    {
        // Take user with email
        $user = $repository->findOneBy(array("username" => $data->getUsername()));
        if ($user !== null && $user->getTokenPassword() === $data->getTokenPassword()) {
            // Create a new Tokencomposer.jsoncomposer.json
            $token = $JWTManager->create($user);
            $user->setTokenPassword($token);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        } else {
            throw new Exception("WRONG TOKEN");
        }

        $mailin = new Mailin("https://api.sendinblue.com/v2.0", "AbXE3BKhWf7mtdTC");

        $data = array(
            "to" => array("alexandrebobichon@gmail.com" => "Alexandre Bobichon"),
            "from" => array("'". $user->getUsername() . "'"),
            "subject" => "Changement de Mot de passse.",
            "html" => "lien pour changer de pseudo <a href='#'>Lien</a>",
            "text" => "Je suis du texte",
            "headers" => array("Content-Type" => "text/html;",)
        );
        var_dump($mailin->send_email($data));
    }

    /**
     * @Route(
     *     name="api_users_password_reset",
     *     path="/users/password/reset",
     *     methods={"POST"},
     *     defaults={
     *         "_api_resource_class"=User::class,
     *         "_api_collection_operation_name"="post"
     *     }
     * )
     * Change user password
     *
     */
    public function resetPassword(User $data, UserPasswordEncoderInterface $encoder, UserRepository $repository)
    {
        // Grep user from database
        $user = $repository->findOneBy(array("username" => $data->getUsername()));
        if ($user !== null){
            $encoded = $encoder->encodePassword($data ,$data->getNewPassword());
            $user->setPassword($encoded);
            $em = $this->getDoctrine()->getManager();

            $em->persist($user);
            $em->flush();

            throw new Exception($user->getUsername());
        }
    }

}