<?php

namespace App\Controller;

use App\Entity\Account;
use App\Entity\InvitationMembership;
use App\Entity\Membership;
use App\Entity\User;
use App\Repository\AccountRepository;
use App\Repository\MembershipRepository;
use App\Repository\UserRepository;
use Sendinblue\Mailin;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class InvitaionController extends AbstractController
{

    /**
     * @Route(
     *     name="api_invitation_user",
     *     path="/invitation_memberships",
     *     methods={"POST"},
     *     defaults={
     *         "_api_resource_class"=User::class,
     *         "_api_collection_operation_name"="post"
     *     }
     * )
     *invition for membership
     *
     */
    public function invitationMembership(UserRepository $repo, UserRepository $userRepository,User $inviter, AccountRepository $accountRepo, Account $account)
    {
        $userInvite = $userRepository->findOneBy(array("username" => $inviter->getUsername()));
        $accountAdmin = $accountRepo->findOneBy(array("id" => $account->getId()));
        $admin = $accountAdmin->getAdminUser();
        if ($admin !== null) {
            $mailin = new Mailin("https://api.sendinblue.com/v2.0", "AbXE3BKhWf7mtdTC");

            $data = array(
                "to" => array(" ' " .$admin->getUser()->getUsername() . " ' " => " ' " . $admin->getUser()->getFirstName() . " " . $admin->getUser()->getLastName(). " ' "),
                "from" => array("'" . $userInvite->getUsername() . "'"),
                "subject" => "Invitation membership.",
                "html" => "invitation pour etre membre du compte <a href='http://localhost/bookinou/api/public/api/invitation_membership/accept_invite' class='btn btn-primary'> link </a>",
                "text" => "Rejoin le compte :^)",
                "headers" => array("Content-Type" => "text/html;",)
            );
                $mailin->send_email($data);
        }

        $em = $this->getDoctrine()->getManager();

        $invit = new InvitationMembership();
        $invit->setConfirm(true);
        $invit->setIdUser($userInvite);
        $invit->getIdAccount($admin->getAccount());

        $em->persist($invit);
        $em->flush();
    }

    /**
     * @Route(
     *     name="api_invitation_user",
     *     path="invitation_membership/accept_invite",
     *     methods={"POST"},
     *     defaults={
     *         "_api_resource_class"=User::class,
     *         "_api_collection_operation_name"="post"
     *     }
     * )
     *invition for membership
     *
     */
    public function acceptInvitation(User $data, InvitationMembership $invitation){

        $em = $this->getDoctrine()->getManager();

        if ($invitation->getIdUser() === $data->getId()){
            $membership = new Membership();
            $membership->setUser($data);
            $membership->setAccount($invitation->getIdAccount());

            $em->persist($membership);
            $em->flush();
        }


    }
}
