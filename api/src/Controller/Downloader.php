<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 25/09/18
 * Time: 18:00
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Aws\S3\S3Client;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;

class Downloader extends Controller {
    /**
     * Master method downloader for jpeg and mp3 file from AWS
     * @Route("/api/downloader/{type}/{file}", requirements={"type"="(pictures|sounds)", "id"="^[a-z0-9]*\.(jpg|jpeg)"}, methods={"GET"})
     * @param String $type [pictures or sounds]
     * @param String $file File name
     * @return Response
     */
    public function downloadPicture($type, $file) {
        $client = new S3Client([
            'credentials' => [
                'key'    => 'XRTQER7CPJ4NPLNIIAHT',
                'secret' => 'YjX3r3Y6VBQVBvfxLF9BKYoAiyI2/pn8IBKqObu7TAM',
            ],
            'region' => '',
            'version' => 'latest',
            'endpoint' => 'https://ams3.digitaloceanspaces.com',
        ]);

        $bucket = 's1-betty';
        $path = 'app/' . $type . '/' . $file;

        if ($client->doesObjectExist($bucket, $path)) {
            $cmd = $client->getCommand('GetObject', [
                'Bucket' => $bucket,
                'Key' => $path
            ]);
            $url = $client->createPresignedRequest($cmd, '+1 minute');

            return new StreamedResponse(function () use ($url, $path) {
                readfile((string) $url->getUri());
            }, 200, [
                'Content-Transfer-Encoding', 'binary',
                'Content-Type' => "application/octet-stream",
                'Content-Disposition' => ('attachment; filename="' . $file . '"')
            ]);
        }

        return new Response('');
    }
}