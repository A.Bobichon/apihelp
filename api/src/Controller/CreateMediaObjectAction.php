<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 24/09/18
 * Time: 16:56
 */

namespace App\Controller;

use ApiPlatform\Core\Bridge\Symfony\Validator\Exception\ValidationException;
use App\Entity\MediaObject;
use App\Form\MediaObjectType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class CreateMediaObjectAction extends Controller {
    private $validator;
    private $doctrine;
    private $factory;

    public function __construct(RegistryInterface $doctrine, FormFactoryInterface $factory, ValidatorInterface $validator) {
        $this->validator = $validator;
        $this->doctrine = $doctrine;
        $this->factory = $factory;
    }

    /**
     * @IsGranted("ROLE_USER_APP")
     */
    public function __invoke(Request $request): MediaObject {
        $mediaObject = new MediaObject();
        $mediaObject->setOwner($this->getUser());
        $mediaObject->setIsActive(true);
        $mediaObject->setUpdatedAt(new \DateTimeImmutable());

        $form = $this->factory->create(MediaObjectType::class, $mediaObject);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $mediaObject->setType($this->getTypeOfFile($mediaObject));
            $em = $this->doctrine->getManager();
            $em->persist($mediaObject);
            $em->flush();

            // Prevent the serialization of the file property
            $mediaObject->file = null;

            return $mediaObject;
        }

        // This will be handled by API Platform and returns a validation error.
        throw new ValidationException($this->validator->validate($mediaObject));
    }

    /**
     * Get type of file [pictures or sounds]
     * @param MediaObject $mediaObject
     * @return String $type
     */
    private function getTypeOfFile (MediaObject $mediaObject) {
        $type = "unknown";
        if ($mediaObject !== null && $mediaObject->getFile() !== null) {
            $mimeType = $mediaObject->getFile()->getMimeType();
            if ($mimeType === 'audio/mpeg') {
                $type = 'sounds';
            } else if ($mimeType === 'image/jpeg') {
                $type = 'pictures';
            }
        }

        return $type;
    }
}