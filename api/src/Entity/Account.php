<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"read_account"}},
 *     denormalizationContext={"groups"={"write_account"}},
 *     collectionOperations={
 *         "post"
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={
 *             "denormalization_context"={"groups"={"put_account"}}
 *         },
 *         "delete"={"access_control"="is_granted('ROLE_BOSS_APP')", "access_control_message"="Only super admin profil can delete entity."}
 *     },
 *     graphql={
 *         "query"
 *     }
 * )
 * @ApiFilter(SearchFilter::class)
 * @ApiFilter(OrderFilter::class)
 * @ORM\Entity(repositoryClass="App\Repository\AccountRepository")
 */
class Account
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=75)
     * @Assert\NotBlank(
     *      message = "Your account name can't be empty"
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 75,
     *      minMessage = "Your account name must be at least {{ limit }} characters long",
     *      maxMessage = "Your account name cannot be longer than {{ limit }} characters"
     * )
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Account name (length: 2-30)", "example"="Ma petite family", "required"=true}
     *     }
     * )
     * @Groups({"read_account", "write_account", "put_account"})
     */
    private $name;

    /**
     * @var MediaObject|null
     * @ORM\ManyToOne(targetEntity="App\Entity\MediaObject")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(
     *      message = "You need a picture for your account"
     * )
     * @Assert\NotNull(
     *      message = "Your picture for your account can't be null"
     * )
     * @ApiProperty(
     *     iri="http://schema.org/MediaObject",
     *     attributes={
     *         "swagger_context"={"description"="Account picture (=> MediaObject)", "example"="api/media_objects/1", "required"=true}
     *     }
     * )
     * @Groups({"read_account", "write_account", "put_account"})
     */
    private $picture;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank(
     *      message = "Your account name can't be empty"
     * )
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Uid device linked to Account", "example"="000CF15698AD", "required"=true}
     *     }
     * )
     * @Groups({"read_account", "write_account", "put_account"})
     *
     */
    private $uidDevice;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Membership", mappedBy="account", cascade={"persist"})
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Membership linked to Account", "example"="api/memberships/1", "required"=false}
     *     }
     * )
     * @Groups({"read_account", "write_account"})
     * Deny PUT on child entities OneToMany, ID of entity is on child
     */
    private $memberships;

    /**
     * @ORM\Column(type="string", length=7, options={"default" : "L5PS3HX"})
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Account code", "example"="L5PS3HX", "required"=false}
     *     }
     * )
     * @Groups({"read_account"})
     */
    private $code;

    /**
     * @ORM\Column(type="datetime")
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Entity updated date", "example"="2018-01-01 12:00:00", "required"=false}
     *     }
     * )
     * @Groups({"read_account"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", options={"default" : true})
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Entity status", "example"=true, "required"=false}
     *     }
     * )
     * @Groups({"read_account", "write_account", "put_account"})
     */
    private $isActive;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\InvitationMembership", mappedBy="idAccount")
     */
    private $invitationMemberships;

    public function __construct()
    {
        $this->storyAccounts = new ArrayCollection();
        $this->memberships = new ArrayCollection();
        $this->invitationMemberships = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPicture(): ?MediaObject
    {
        return $this->picture;
    }

    public function setPicture(?MediaObject $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getUidDevice(): ?string
    {
        return $this->uidDevice;
    }

    public function setUidDevice(string $uidDevice): self
    {
        $this->uidDevice = $uidDevice;

        return $this;
    }

    public function getAdminUser(): ?User
    {
        return $this->adminUser;
    }

    public function setAdminUser(?User $adminUser): self
    {
        $this->adminUser = $adminUser;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection|Membership[]
     */
    public function getMemberships(): Collection
    {
        return $this->memberships;
    }

    public function addMembership(Membership $membership): self
    {
        if (!$this->memberships->contains($membership)) {
            $this->memberships[] = $membership;
            $membership->setAccount($this);
        }

        return $this;
    }

    public function removeMembership(Membership $membership): self
    {
        if ($this->memberships->contains($membership)) {
            $this->memberships->removeElement($membership);
            // set the owning side to null (unless already changed)
            if ($membership->getAccount() === $this) {
                $membership->setAccount(null);
            }
        }

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Collection|InvitationMembership[]
     */
    public function getInvitationMemberships(): Collection
    {
        return $this->invitationMemberships;
    }

    public function addInvitationMembership(InvitationMembership $invitationMembership): self
    {
        if (!$this->invitationMemberships->contains($invitationMembership)) {
            $this->invitationMemberships[] = $invitationMembership;
            $invitationMembership->setIdAccount($this);
        }

        return $this;
    }

    public function removeInvitationMembership(InvitationMembership $invitationMembership): self
    {
        if ($this->invitationMemberships->contains($invitationMembership)) {
            $this->invitationMemberships->removeElement($invitationMembership);
            // set the owning side to null (unless already changed)
            if ($invitationMembership->getIdAccount() === $this) {
                $invitationMembership->setIdAccount(null);
            }
        }

        return $this;
    }
}
