<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"read_tag"}},
 *     denormalizationContext={"groups"={"write_tag"}},
 *     collectionOperations={
 *         "post"
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={
 *             "denormalization_context"={"groups"={"put_tag"}}
 *         },
 *         "delete"={"access_control"="is_granted('ROLE_BOSS_APP')", "access_control_message"="Only super admin profil can delete entity."}
 *     },
 *     graphql={
 *         "query"
 *     }
 * )
 * @ApiFilter(SearchFilter::class)
 * @ApiFilter(OrderFilter::class)
 * @ORM\Entity(repositoryClass="App\Repository\TagRepository")
 */
class Tag
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=75, nullable=false)
     * @Assert\NotNull(
     *      message = "Your tag uid can't be null"
     * )
     * @Assert\NotBlank(
     *      message = "Your tag uid can't be empty"
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 75,
     *      minMessage = "Your tag uid must be at least {{ limit }} characters long",
     *      maxMessage = "Your tag uid cannot be longer than {{ limit }} characters"
     * )
     * @Assert\NotBlank(
     *      message = "Your tag uid can't be empty"
     * )
     * @Groups({"read_tag", "write_tag", "put_tag"})
     */
    private $uid;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Story", inversedBy="tags")
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Story linked to Story", "example"="api/stories/1", "required"=true}
     *     }
     * )
     * @Groups({"read_tag", "write_tag", "put_tag"})
     */
    private $story;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="stories")
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Story owner", "example"="api/users/1", "required"=true}
     *     }
     * )
     * @Groups({"read_tag"} )
     */
    public $owner;

    /**
     * @ORM\Column(type="boolean", options={"default" : true})
     * @Groups({"read_tag", "write_tag", "put_tag"})
     */
    private $isActive;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read_tag"})
     */
    private $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUid(): ?string
    {
        return $this->uid;
    }

    public function setUid(string $uid): self
    {
        $this->uid = $uid;

        return $this;
    }

    public function getStory(): ?Story
    {
        return $this->story;
    }

    public function setStory(?Story $story): self
    {
        $this->story = $story;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
