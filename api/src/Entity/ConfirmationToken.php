<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ConfirmationTokenRepository")
 */
class ConfirmationToken
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="confirmationTokens")
     */
    private $toto;

    /**
     * @ORM\Column(type="text")
     */
    private $tokenConfirmation;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateExpiration;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getToto(): ?User
    {
        return $this->toto;
    }

    public function setToto(?User $toto): self
    {
        $this->toto = $toto;

        return $this;
    }

    public function getTokenConfirmation(): ?string
    {
        return $this->tokenConfirmation;
    }

    public function setTokenConfirmation(string $tokenConfirmation): self
    {
        $this->tokenConfirmation = $tokenConfirmation;

        return $this;
    }

    public function getDateExpiration(): ?\DateTimeInterface
    {
        return $this->dateExpiration;
    }

    public function setDateExpiration(\DateTimeInterface $dateExpiration): self
    {
        $this->dateExpiration = $dateExpiration;

        return $this;
    }
}
