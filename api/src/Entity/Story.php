<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"read_story"}},
 *     denormalizationContext={"groups"={"write_story"}},
 *     collectionOperations={
 *         "post"
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={
 *             "denormalization_context"={"groups"={"put_story"}}
 *         },
 *         "delete"={"access_control"="is_granted('ROLE_BOSS_APP')", "access_control_message"="Only super admin profil can delete entity."}
 *     },
 *     graphql={
 *         "query",
 *          "delete",
 *          "update",
 *          "create"
 *     }
 * )
 * @ApiFilter(SearchFilter::class)
 * @ApiFilter(OrderFilter::class)
 * @ORM\Entity(repositoryClass="App\Repository\StoryRepository")
 */
class Story
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     * @Assert\NotNull(
     *      message = "Your story title can't be null"
     * )
     * @Assert\NotBlank(
     *      message = "Your story title can't be empty"
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 45,
     *      minMessage = "Your title must be at least {{ limit }} characters long",
     *      maxMessage = "Your title cannot be longer than {{ limit }} characters"
     * )
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Story title (length: 2-45)", "example"="Léo chez le docteur", "required"=true}
     *     }
     * )
     * @Groups({"read_story", "write_story", "put_story"})
     */
    private $title;

    /**
     * @var MediaObject|null
     * @ORM\ManyToOne(targetEntity="App\Entity\MediaObject")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(
     *        message = "Your picture for story can't be null"
     *  )
     * @ApiProperty(
     *     iri="http://schema.org/MediaObject",
     *     attributes={
     *         "swagger_context"={"description"="Story picture (=> MediaObject)", "example"="api/media_objects/1", "required"=true}
     *     }
     * )
     * @Groups({"read_story", "write_story", "put_story"})
     */
    private $picture;

    /**
     * @var MediaObject|null
     * @ORM\ManyToOne(targetEntity="App\Entity\MediaObject")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(
     *        message = "Your picture for story can't be empty"
     *  )
     * @Assert\NotNull(
     *        message = "Your picture for story can't be null"
     *  )
     * @ApiProperty(
     *     iri="http://schema.org/MediaObject",
     *     attributes={
     *         "swagger_context"={"description"="Story sound (=> MediaObject)", "example"="api/media_objects/1", "required"=true}
     *     }
     * )
     * @Groups({"read_story", "write_story", "put_story"})
     */
    private $sound;

    /**
     * @ORM\Column(type="time")
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Story duration", "example"="00:03:13", "required"=true}
     *     }
     * )
     * @Assert\NotBlank(
     *        message = "Your sound for story can't be empty"
     *  )
     * @Assert\NotNull(
     *        message = "Your sound for story can't be null"
     *  )
     * @Groups({"read_story", "write_story", "put_story"})
     */
    private $duration;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tag", mappedBy="story", orphanRemoval=true)
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Story tags", "example"="api/tags/1", "required"=false}
     *     }
     * )
     * @Groups({"read_story", "write_story"})
     * Deny PUT on child entities OneToMany, ID of entity is on child
     */
    private $tags;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="stories")
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Story owner", "example"="api/users/1", "required"=true}
     *     }
     * )
     * @Groups({"read_story"})
     */
    public $owner;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Library", mappedBy="story")
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Library linked to Story", "example"="api/libraries/1", "required"=true}
     *     }
     * )
     * @Groups({"read_story", "write_story"})
     * Deny PUT on child entities OneToMany, ID of entity is on child
     */
    private $libraries;

    /**
     * @ORM\Column(type="datetime")
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Entity updated date", "example"="2018-01-01 12:00:00", "required"=false}
     *     }
     * )
     * @Groups({"read_story"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", options={"default" : true})
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Entity status", "example"=true, "required"=false}
     *     }
     * )
     * @Groups({"read_story", "write_story", "put_story"})
     */
    private $isActive;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->libraries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDuration(): ?\DateTimeInterface
    {
        return $this->duration;
    }

    public function setDuration(\DateTimeInterface $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getPicture(): ?MediaObject
    {
        return $this->picture;
    }

    public function setPicture(?MediaObject $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getSound(): ?MediaObject
    {
        return $this->sound;
    }

    public function setSound(?MediaObject $sound): self
    {
        $this->sound = $sound;

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
            $tag->setStory($this);
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            // set the owning side to null (unless already changed)
            if ($tag->getStory() === $this) {
                $tag->setStory(null);
            }
        }

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection|Library[]
     */
    public function getLibraries(): Collection
    {
        return $this->libraries;
    }

    public function addLibraries(Library $library): self
    {
        if (!$this->libraries->contains($library)) {
            $this->libraries[] = $library;
            $library->setStory($this);
        }

        return $this;
    }

    public function removeLibraries(Library $library): self
    {
        if ($this->libraries->contains($library)) {
            $this->libraries->removeElement($library);
            // set the owning side to null (unless already changed)
            if ($library->getStory() === $this) {
                $library->setStory(null);
            }
        }

        return $this;
    }
}
