<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"read_library"}},
 *     denormalizationContext={"groups"={"write_library"}},
 *     collectionOperations={
 *         "post"
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={
 *             "denormalization_context"={"groups"={"put_library"}}
 *         },
 *         "delete"={"access_control"="is_granted('ROLE_BOSS_APP')", "access_control_message"="Only super admin profil can delete entity."}
 *     },
 *     graphql={
 *         "query"
 *     }
 * )
 * @ApiFilter(SearchFilter::class)
 * @ApiFilter(OrderFilter::class)
 * @ORM\Entity(repositoryClass="App\Repository\LibraryRepository")
 * @ORM\Table(
 *    uniqueConstraints={
 *        @UniqueConstraint(name="library_unique",
 *            columns={"membership_id", "owner_id", "story_id"})
 *    }
 * )
 * @UniqueEntity(fields={"membership", "owner", "story"}, message="Duplicated entry.")
 */
class Library
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Story", inversedBy="libraries", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Story linked to Library", "example"="api/stories/1", "required"=true}
     *     }
     * )
     * @Groups({"read_library", "write_library", "put_library"})
     */
    private $story;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Membership", inversedBy="libraries")
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Membership linked to Library", "example"="api/memberships/1", "required"=true}
     *     }
     * )
     * @Groups({"read_library", "write_library", "put_library"})
     */
    private $membership;

    /**
     * @ORM\Column(type="boolean", options={"default" : false})
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="status of Story in Account", "example"="true"}
     *     }
     * )
     * @Groups({"read_library", "write_library", "put_library"})
     */
    private $isTransfered;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="stories")
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Library owner", "example"="api/users/1", "required"=true}
     *     }
     * )
     * @Groups({"read_library"})
     */
    public $owner;

    /**
     * @ORM\Column(type="datetime")
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Entity updated date", "example"="2018-01-01 12:00:00", "required"=false}
     *     }
     * )
     * @Groups({"read_library"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", options={"default" : true})
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Entity status", "example"="true", "required"=false}
     *     }
     * )
     * @Groups({"read_library", "write_library", "put_library"})
     */
    private $isActive;

    public function __construct()
    {
        $this->memberships = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStory(): ?Story
    {
        return $this->story;
    }

    public function setStory(?Story $story): self
    {
        $this->story = $story;

        return $this;
    }

    public function getMembership(): ?Membership
    {
        return $this->membership;
    }

    public function setMembership(?Membership $membership): self
    {
        $this->membership = $membership;

        return $this;
    }

    public function getIsTransfered(): ?bool
    {
        return $this->isTransfered;
    }

    public function setIsTransfered(bool $isTransfered): self
    {
        $this->isTransfered = $isTransfered;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
}
