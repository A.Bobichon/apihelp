<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\CreateMediaObjectAction;
use Aws\S3\S3Client;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     iri="http://schema.org/MediaObject",
 *     normalizationContext={"groups"={"read_media_object"}},
 *     denormalizationContext={"groups"={"write_media_object"}},
 *     collectionOperations={
 *         "post"={
 *              "method"="POST",
 *              "path"="/media_objects",
 *              "controller"=CreateMediaObjectAction::class,
 *              "defaults"={"_api_receive"=false}
 *          }
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={
 *             "denormalization_context"={"groups"={"put_media_object"}}
 *         },
 *         "delete"={"access_control"="is_granted('ROLE_BOSS_APP')", "access_control_message"="Only super admin profil can delete entity."}
 *     },
 *     graphql={
 *         "query",
 *         "mutation"
 *     }
 * )
 * @ApiFilter(SearchFilter::class)
 * @ApiFilter(OrderFilter::class)
 * @ORM\Entity
 * @Vich\Uploadable
 */
class MediaObject {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var File|null
     * @Assert\NotNull(
     *      message = "Your media_object file can't be null"
     * )
     * @Assert\NotBlank(
     *      message = "Your media_object file can't be empty"
     * )
     * @Assert\File(
     *     maxSize = "5M",
     *     maxSizeMessage = "The file is too large ({{ size }} {{ suffix }}). Allowed maximum size is {{ limit }} {{ suffix }}",
     *     mimeTypes = {"image/jpeg", "audio/mpeg"},
     *     mimeTypesMessage = "The mime type of the file is invalid ({{ type }}). Allowed mime types are {{ types }}"
     * )
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="MediaObject file", "required"=true}
     *     }
     * )
     * @Groups({"write_media_object"})
     * @Vich\UploadableField(mapping="media_object", fileNameProperty="contentUrl")
     */
    public $file;

    /**
     * @var string|null
     * @ORM\Column(nullable=true)
     * @ApiProperty(iri="http://schema.org/contentUrl")
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="MediaObject url", "required"=false}
     *     }
     * )
     * @Groups({"read_media_object"})
     */
    public $contentUrl;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="MediaObject owner", "example"="api/users/1", "required"=true}
     *     }
     * )
     * @Groups({"read_media_object"})
     */
    private $owner;

    /**
     * @ORM\Column(type="datetime")
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Entity updated date", "example"="2018-01-01 12:00:00", "required"=false}
     *     }
     * )
     * @Groups({"read_media_object"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", options={"default" : true})
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Entity status", "example"=true, "required"=false}
     *     }
     * )
     * @Groups({"read_media_object", "write_media_object", "put_media_object"})
     */
    private $isActive;

    /**
     * @ORM\Column(type="string", length=30)
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Entity type [sound, image]", "example"="picture", "required"=false}
     *     }
     * )
     */
    private $type;

    /**
     * @return null|string
     */
    public function getContentUrl() {
        $client = new S3Client([
            'credentials' => [
                'key'    => 'XRTQER7CPJ4NPLNIIAHT',
                'secret' => 'YjX3r3Y6VBQVBvfxLF9BKYoAiyI2/pn8IBKqObu7TAM',
            ],
            'region' => '',
            'version' => 'latest',
            'endpoint' => 'https://ams3.digitaloceanspaces.com',
        ]);

        $path = 'app/pictures/' . $this->contentUrl;
        if ($client->doesObjectExist('s1-betty', $path)) {
            $cmd = $client->getCommand('GetObject', [
                'Bucket' => 's1-betty',
                'Key' => $path
            ]);
            $url = $client->createPresignedRequest($cmd, '+30 minute');

            return (string) $url->getUri();
        }

        return "toto";
    }

    public function getId() {
        return $this->id;
    }

    /**
     * @return null|File
     */
    public function getFile() {
        return $this->file;
    }

    /**
     * @param null|File $file
     */
    public function setFile($file) {
        $this->file = $file;
    }

    public function setContentUrl(?string $contentUrl): self
    {
        $this->contentUrl = $contentUrl;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }
}