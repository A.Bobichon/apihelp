<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"read_membership", "read_account"}},
 *     denormalizationContext={"groups"={"write_membership", "write_account"}},
 *     collectionOperations={
 *         "post"
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={
 *             "access_control_message"="Only owner can update entity.",
 *             "denormalization_context"={"groups"={"put_membership"}}
 *         },
 *         "delete"={"access_control"="is_granted('ROLE_BOSS_APP')", "access_control_message"="Only super admin profil can delete entity."}
 *     },
 *     graphql={
 *         "query"
 *     }
 * )
 * @ApiFilter(SearchFilter::class)
 * @ApiFilter(OrderFilter::class)
 * @ORM\Entity(repositoryClass="App\Repository\MembershipRepository")
 */
class Membership
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="memberships")
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Membership owner", "example"="api/users/1", "required"=true}
     *     }
     * )
     * @Groups({"read_membership"})
     */
    public $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Account", inversedBy="memberships", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Account linked to Membership", "example"="api/accounts/1", "required"=true}
     *     }
     * )
     * @Groups({"read_membership", "read_account", "write_membership", "write_account", "put_membership"})
     */
    private $account;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Library", mappedBy="membership")
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Library linked to Membership", "example"="api/libraries/1", "required"=true}
     *     }
     * )
     * @Groups({"read_membership", "write_membership"})
     * Deny PUT on child entities OneToMany, ID of entity is on child
     */
    private $libraries;

    /**
     * @ORM\Column(type="string", length=35)
     * @Assert\NotNull(
     *      message = "Your membership role can't be null"
     * )
     * @Assert\NotBlank(
     *      message = "Your membership role can't be empty"
     * )
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="User role on account", "example"="Papi, Maman, etc.", "required"=true}
     *     }
     * )
     * @Groups({"read_membership", "write_membership", "put_membership"})
     * @Assert\Length(
     *      min = 2,
     *      max = 35,
     *      minMessage = "Your role must be at least {{ limit }} characters long",
     *      maxMessage = "Your role cannot be longer than {{ limit }} characters"
     * )
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="User pseudo on account", "example"="Papili", "required"=false}
     *     }
     * )
     * @Groups({"read_membership", "write_membership", "put_membership"})
     * @Assert\Length(
     *      min = 3,
     *      max = 45,
     *      minMessage = "Your pseudo must be at least {{ limit }} characters long",
     *      maxMessage = "Your pseudo cannot be longer than {{ limit }} characters"
     * )
     */
    private $pseudo;

    /**
     * @ORM\Column(type="datetime")
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Entity updated date", "example"="2018-01-01 12:00:00", "required"=false}
     *     }
     * )
     * @Groups({"read_membership"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean")
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Entity status", "example"="true", "required"=false}
     *     }
     * )
     * @Groups({"read_membership", "write_membership", "put_membership"})
     */
    private $isActive;

    /**
     * @ORM\Column(type="boolean", options={"default" : false})
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="User role on account", "example"="true", "required"=false}
     *     }
     * )
     * @Groups({"read_membership", "write_membership", "put_membership"})
     */
    private $isAdmin;

    public function __construct()
    {
        $this->setUpdatedAt(new \DateTimeImmutable());
        $this->setIsActive(true);
        $this->libraries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(?Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection|Library[]
     */
    public function getLibraries(): Collection
    {
        return $this->libraries;
    }

    public function addLibraries(Library $library): self
    {
        if (!$this->libraries->contains($library)) {
            $this->libraries[] = $library;
            $library->setMembership($this);
        }

        return $this;
    }

    public function removeLibraries(Library $library): self
    {
        if ($this->libraries->contains($library)) {
            $this->libraries->removeElement($library);
            // set the owning side to null (unless already changed)
            if ($library->getMembership() === $this) {
                $library->setMembership(null);
            }
        }

        return $this;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(?string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getIsAdmin(): ?bool
    {
        return $this->isAdmin;
    }

    public function setIsAdmin(bool $isAdmin): self
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }
}
