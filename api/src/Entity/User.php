<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"read_user"}},
 *     denormalizationContext={"groups"={"write_user"}},
 *     collectionOperations={
 *         "post"
 *     },
 *     itemOperations={
 *         "get"={"access_control"="is_granted('ROLE_USER_APP')"},
 *         "put"={
 *             "access_control"="is_granted('ROLE_USER_APP')",
 *             "denormalization_context"={"groups"={"put_user"}}
 *         },
 *         "delete"={"access_control"="is_granted('ROLE_BOSS_APP')", "access_control_message"="Only super admin profil can delete entity."}
 *     },
 *     graphql={
 *         "query"
 *     }
 * )
 * @ApiFilter(SearchFilter::class)
 * @ApiFilter(OrderFilter::class)
 * @ORM\Table(name="`user`")
 * @UniqueEntity("username")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotNull(
     *      message = "Your user username can't be null"
     * )
     * @Assert\NotBlank(
     *      message = "Your user username can't be empty"
     * )
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     * @Assert\Length(
     *      min = 5,
     *      max = 255,
     *      minMessage = "Your email must be at least {{ limit }} characters long",
     *      maxMessage = "Your email cannot be longer than {{ limit }} characters"
     * )
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="User id (valid MX email)", "example"="dupont@gmail.com", "required"=true},
     *
     *     }
     * )
     * @Groups({"read_user", "write_user", "put_user"})
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull(
     *      message = "Your user password can't be null"
     * )
     * @Assert\NotBlank(
     *      message = "Your user password can't be empty"
     * )
     * @Assert\Length(
     *      min = 5,
     *      max = 255,
     *      minMessage = "Your password must be at least {{ limit }} characters long",
     *      maxMessage = "Your password cannot be longer than {{ limit }} characters"
     * )
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="User password (length: 5-30)", "example"="***********", "required"=true}
     *     }
     * )
     * @Groups({"write_user"})
     */
    private $password;

    /**
     * @ApiProperty()
     * @Groups({"write_user"})
     * @Assert\Length(
     *      min = 5,
     *      max = 255,
     *      minMessage = "Your newPassword must be at least {{ limit }} characters long",
     *      maxMessage = "Your newPassword cannot be longer than {{ limit }} characters"
     * )
     */
    private $newPassword;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotNull(
     *      message = "Your user firstname can't be null"
     * )
     * @Assert\NotBlank(
     *      message = "Your user firstname can't be empty"
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Your firstname must be at least {{ limit }} characters long",
     *      maxMessage = "Your firstname cannot be longer than {{ limit }} characters"
     * )
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="User firstname (length: 2-50)", "example"="Jean", "required"=true}
     *     }
     * )
     * @Groups({"read_user", "write_user", "put_user"})
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=75)
     * @Assert\NotNull(
     *      message = "Your user lastname can't be null"
     * )
     * @Assert\NotBlank(
     *      message = "Your user lastname can't be empty"
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 75,
     *      minMessage = "Your lastname must be at least {{ limit }} characters long",
     *      maxMessage = "Your lastname cannot be longer than {{ limit }} characters"
     * )
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="User lastname (length: 2-75)", "example"="Dupont", "required"=true}
     *     }
     * )
     * @Groups({"read_user", "write_user", "put_user"})
     */
    private $lastName;

    /**
     * @var MediaObject|null
     * @ORM\ManyToOne(targetEntity="App\Entity\MediaObject")
     * @ORM\JoinColumn(nullable=true)
     * @ApiProperty(
     *     iri="http://schema.org/MediaObject",
     *     attributes={
     *         "swagger_context"={"description"="User picture (=> MediaObject)", "example"="api/media_objects/1", "required"=false}
     *     }
     * )
     * @Groups({"read_user", "write_user", "put_user"})
     */
    private $picture;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\LessThan("today")
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="User birth date (less than now)", "example"="2018-01-01", "required"=false}
     *     }
     * )
     * @Groups({"read_user", "write_user", "put_user"})
     */
    private $birthDate;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=30, options={"default" : "user"})
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="User type [user or publisher]", "example"="user", "required"=true}
     *     }
     * )
     * @Groups({"read_user", "write_user", "put_user"})
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Membership", mappedBy="user")
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Account link", "example"="api/memberships/1", "required"=false}
     *     }
     * )
     * @Groups({"read_user", "write_user"})
     * Deny PUT on child entities OneToMany, ID of entity is on child
     */
    private $memberships;

    /**
     * @ORM\Column(type="datetime")
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Entity updated date", "example"="2018-01-01 12:00:00", "required"=false}
     *     }
     * )
     * @Groups({"read_user"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", options={"default" : true})
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"description"="Entity status", "example"="true", "required"=false}
     *     }
     * )
     * @Groups({"read_user", "write_user", "put_user"})
     */
    private $isActive;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ConfirmationToken", mappedBy="toto")
     */
    private $confirmationTokens;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\InvitationMembership", mappedBy="idUser")
     */
    private $invitationMemberships;



    public function __construct()
    {
        $this->memberships = new ArrayCollection();
        $this->confirmationTokens = new ArrayCollection();
        $this->invitationMemberships = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getNewPassword() {
        return $this->newPassword;
    }

    public function setNewPassword($newPassword) {
        $this->newPassword = $newPassword;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPicture(): ?MediaObject
    {
        return $this->picture;
    }

    public function setPicture(?MediaObject $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(?\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection|Membership[]
     */
    public function getMemberships(): Collection
    {
        return $this->memberships;
    }

    public function addMembership(Membership $membership): self
    {
        if (!$this->memberships->contains($membership)) {
            $this->memberships[] = $membership;
            $membership->setMember($this);
        }

        return $this;
    }

    public function removeMembership(Membership $membership): self
    {
        if ($this->memberships->contains($membership)) {
            $this->memberships->removeElement($membership);
            // set the owning side to null (unless already changed)
            if ($membership->getMember() === $this) {
                $membership->setMember(null);
            }
        }

        return $this;
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize() {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized) {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized, array('allowed_classes' => false));
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles() {
        return [$this->role];
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt() {
        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials() {
        // TODO: Implement eraseCredentials() method.
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|ConfirmationToken[]
     */
    public function getConfirmationTokens(): Collection
    {
        return $this->confirmationTokens;
    }

    public function addConfirmationToken(ConfirmationToken $confirmationToken): self
    {
        if (!$this->confirmationTokens->contains($confirmationToken)) {
            $this->confirmationTokens[] = $confirmationToken;
            $confirmationToken->setToto($this);
        }

        return $this;
    }

    public function removeConfirmationToken(ConfirmationToken $confirmationToken): self
    {
        if ($this->confirmationTokens->contains($confirmationToken)) {
            $this->confirmationTokens->removeElement($confirmationToken);
            // set the owning side to null (unless already changed)
            if ($confirmationToken->getToto() === $this) {
                $confirmationToken->setToto(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection|InvitationMembership[]
     */
    public function getInvitationMemberships(): Collection
    {
        return $this->invitationMemberships;
    }

    public function addInvitationMembership(InvitationMembership $invitationMembership): self
    {
        if (!$this->invitationMemberships->contains($invitationMembership)) {
            $this->invitationMemberships[] = $invitationMembership;
            $invitationMembership->setIdUser($this);
        }

        return $this;
    }

    public function removeInvitationMembership(InvitationMembership $invitationMembership): self
    {
        if ($this->invitationMemberships->contains($invitationMembership)) {
            $this->invitationMemberships->removeElement($invitationMembership);
            // set the owning side to null (unless already changed)
            if ($invitationMembership->getIdUser() === $this) {
                $invitationMembership->setIdUser(null);
            }
        }

        return $this;
    }
}
