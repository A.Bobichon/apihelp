<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 29/09/18
 * Time: 11:48
 */

namespace App\Storage;


use App\Exception\NotFoundException;
use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Storage\AbstractStorage;

class DigitalOceanStorage extends AbstractStorage {
    protected function doUpload(PropertyMapping $mapping, UploadedFile $file, ?string $dir, string $name) {
        $extension = $file->guessExtension();
        if ($extension === 'mp3') {
            $path = 'app/sounds/' . $name;
        } else if ($extension === 'jpg' || $extension === 'jpeg') {
            $path = 'app/pictures/' . $name;
        } else {
            return;
        }

        // Upload to DigitalOcean spaces (AWS S3)

        $client = new S3Client([
            'credentials' => [
                'key'    => 'XRTQER7CPJ4NPLNIIAHT',
                'secret' => 'YjX3r3Y6VBQVBvfxLF9BKYoAiyI2/pn8IBKqObu7TAM',
            ],
            'region' => '',
            'version' => 'latest',
            'endpoint' => 'https://ams3.digitaloceanspaces.com',
        ]);

        $adapter = new AwsS3Adapter($client, 's1-betty');

        $filesystem = new Filesystem($adapter);
        $stream = fopen($file->getRealPath(), 'r+');
        $filesystem->writeStream($path, $stream);
        fclose($stream);
    }

    protected function doRemove(PropertyMapping $mapping, ?string $dir, string $name): ?bool {
        // TODO: Implement doRemove() method.
    }

    /**
     * Do resolve path.
     *
     * @param PropertyMapping $mapping The mapping representing the field
     * @param string $dir The directory in which the file is uploaded
     * @param string $name The file name
     * @param bool $relative Whether the path should be relative or absolute
     *
     * @return string
     */
    protected function doResolvePath(PropertyMapping $mapping, ?string $dir, string $name, ?bool $relative = false): string {
        return $name;
    }
}