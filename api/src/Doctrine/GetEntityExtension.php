<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 09/10/18
 * Time: 01:13
 */

namespace App\Doctrine;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Account;
use App\Entity\Library;
use App\Entity\MediaObject;
use App\Entity\Membership;
use App\Entity\Story;
use App\Entity\Tag;
use App\Entity\User;
use App\Exception\NotFoundException;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


final class GetEntityExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface {
    private $tokenStorage;
    private $authorizationChecker;

    public function __construct(TokenStorageInterface $tokenStorage, AuthorizationCheckerInterface $checker) {
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $checker;
    }

    /**
     * {@inheritdoc}
     */
    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null) {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    /**
     * {@inheritdoc}
     */
    public function applyToItem(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, array $identifiers, string $operationName = null, array $context = []) {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    /**
     * Add controls for getting entities
     * @param QueryBuilder $queryBuilder
     * @param string       $resourceClass
     */
    private function addWhere(QueryBuilder $queryBuilder, string $resourceClass) {
        if (!$this->authorizationChecker->isGranted('ROLE_BOSS_APP')) {
            $rootAlias = $queryBuilder->getRootAliases()[0];
            $queryBuilder->andWhere(sprintf('%s.isActive = true', $rootAlias));

            // Specific entity extension
            switch ($resourceClass) {
                case User::class:
                    $this->userExtension($queryBuilder);
                    break;
                case Story::class:
                    $this->storyExtension($queryBuilder);
                    break;
                case Account::class:
                    $this->accountExtension($queryBuilder);
                    break;
                case Membership::class:
                    $this->membershipExtension($queryBuilder);
                    break;
                case Library::class:
                    $this->libraryExtension($queryBuilder);
                    break;
                case MediaObject::class:
                    $this->mediaObjectExtension($queryBuilder);
                    break;
                case Tag::class:
                    $this->tagExtension($queryBuilder);
                    break;
            }
        }
    }

    /**
     * User extension ORM
     * {@inheritdoc}
     */
    private function userExtension(QueryBuilder $queryBuilder) {
        $user = $this->tokenStorage->getToken()->getUser();
        if ($user instanceof User) {
            $rootAlias = $queryBuilder->getRootAliases()[0];
            $queryBuilder->andWhere(sprintf('%s.id = :current_user', $rootAlias));
            $queryBuilder->setParameter('current_user', $user->getId());
        }
    }

    /**
     * Story extension ORM
     * {@inheritdoc}
     */
    private function storyExtension(QueryBuilder $queryBuilder) {
        $user = $this->tokenStorage->getToken()->getUser();
        if ($user instanceof User) {
            $rootAlias = $queryBuilder->getRootAliases()[0];
            $queryBuilder->andWhere(sprintf('%s.owner = :current_user', $rootAlias));
            $queryBuilder->setParameter('current_user', $user->getId());

            /*
            // Check if library is linked to user account
            $queryBuilder->andWhere(
                $queryBuilder->expr()->in(
                    ':current_user',
                    (new QueryBuilder($queryBuilder->getEntityManager()))->select('identity(ms.user)')
                        ->from('App\Entity\Membership', 'ms')
                        ->where('identity(ms.account) = (' .
                            (new QueryBuilder($queryBuilder->getEntityManager()))->select('identity(ms_sub.account)')
                                ->from('App\Entity\Membership', 'ms_sub')
                                ->innerJoin('ms_sub.libraries', 'l', sprintf('l.story = %s', $rootAlias))
                                ->getDQL()
                            . ')')->getDQL()
                )
            );
            $queryBuilder->setParameter('current_user', $user->getId());
            //throw new NotFoundException($queryBuilder->getDQL());
            */
        }
    }

    /**
     * Account extension ORM
     * {@inheritdoc}
     */
    private function accountExtension(QueryBuilder $queryBuilder) {
        $user = $this->tokenStorage->getToken()->getUser();
        if ($user instanceof User) {
            $rootAlias = $queryBuilder->getRootAliases()[0];

            // Check if current user is on account
            $queryBuilder->andWhere(
                $queryBuilder->expr()->in(
                    ':current_user',
                    (new QueryBuilder($queryBuilder->getEntityManager()))->select('identity(ms.user)')
                        ->from('App\Entity\Membership', 'ms')
                        ->where(sprintf('identity(ms.account) = %s.id', $rootAlias))
                        ->getDQL()
                )
            );
            $queryBuilder->setParameter('current_user', $user->getId());
        }
    }

    /**
     * Membership extension ORM
     * {@inheritdoc}
     */
    private function membershipExtension(QueryBuilder $queryBuilder) {
        $user = $this->tokenStorage->getToken()->getUser();
        if ($user instanceof User) {
            $rootAlias = $queryBuilder->getRootAliases()[0];

            // Check if membership is linked to user account
            $queryBuilder->andWhere(
                $queryBuilder->expr()->in(
                    ':current_user',
                    (new QueryBuilder($queryBuilder->getEntityManager()))->select('identity(ms.user)')
                        ->from('App\Entity\Membership', 'ms')
                        ->where('identity(ms.account) = (' .
                            (new QueryBuilder($queryBuilder->getEntityManager()))->select('identity(ms_sub.account)')
                                ->from('App\Entity\Membership', 'ms_sub')
                                ->where(sprintf('ms_sub = %s', $rootAlias))
                                ->getDQL()
                            . ')')->getDQL()
                )
            );
            $queryBuilder->setParameter('current_user', $user->getId());
            //throw new NotFoundException($queryBuilder->getDQL());
        }
    }

    /**
     * Library extension ORM
     * {@inheritdoc}
     */
    private function libraryExtension(QueryBuilder $queryBuilder) {
        $user = $this->tokenStorage->getToken()->getUser();
        if ($user instanceof User) {
            $rootAlias = $queryBuilder->getRootAliases()[0];

            // Check if library is linked to user account
            /*
            $queryBuilder->andWhere(
                $queryBuilder->expr()->in(
                    ':current_user',
                    (new QueryBuilder($queryBuilder->getEntityManager()))->select('identity(ms.user)')
                        ->from('App\Entity\Membership', 'ms')
                        ->where('identity(ms.account) = (' .
                            (new QueryBuilder($queryBuilder->getEntityManager()))->select('identity(ms_sub.account)')
                                ->from('App\Entity\Membership', 'ms_sub')
                                ->where(sprintf('ms_sub = %s.membership', $rootAlias))
                                ->getDQL()
                        . ')')->getDQL()
                )
            );
            $queryBuilder->setParameter('current_user', $user->getId());
            //throw new NotFoundException($queryBuilder->getDQL());
            */
        }
    }

    /**
     * MediaObject extension ORM
     * {@inheritdoc}
     */
    private function mediaObjectExtension(QueryBuilder $queryBuilder) {
        $user = $this->tokenStorage->getToken()->getUser();
        if ($user instanceof User) {
            $rootAlias = $queryBuilder->getRootAliases()[0];
            $queryBuilder->andWhere(sprintf('%s.owner = :current_user', $rootAlias));
            $queryBuilder->setParameter('current_user', $user->getId());
        }
    }

    /**
     * Tag extension ORM
     * {@inheritdoc}
     */
    private function tagExtension(QueryBuilder $queryBuilder) {
        $user = $this->tokenStorage->getToken()->getUser();
        if ($user instanceof User) {
            $rootAlias = $queryBuilder->getRootAliases()[0];
            $queryBuilder->andWhere(sprintf('%s.owner = :current_user', $rootAlias));
            $queryBuilder->setParameter('current_user', $user);
        }
    }
}