<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;

class LibraryTest extends TestCase
{
    /* @var $user User */
    private $user = null;
    /* @var $story Story */
    private $story = null;
    /* @var $account Account */
    private $account = null;
    /* @var $membership Membership */
    private $membership = null;
    /* @var $library Library */
    private $library = null;

    public function SetUp()
    {
        // Step 1: Create user
        $this->user = new User();
        $this->user->create();
        $this->user->login();

        // Step 2: Create media
        $picture = new MediaObject();
        $picture->create($this->user);

        $sound = new MediaObject();
        $sound->setFile("sounds/04E4CA0A542880.mp3")->create($this->user);

        // Step 3: Create story
        $this->story = new Story();
        $this->story->setPicture($picture);
        $this->story->setSound($sound);
        $this->story->create($this->user);

        // Step 4: Create Account for membership
        $this->account = new Account();
        $this->account->setPicture($picture);
        $this->account->create($this->user);

        // Step 5: Create membership
        $this->membership = new Membership();
        $this->membership->setAccount($this->account);
        $this->membership->create($this->user);

        // Step 6: Initialize Library
        $this->library = new Library();
        $this->library->setMembership($this->membership);
        $this->library->setStory($this->story);
    }

    /**
     * Create library with good data
     */
    public function testNominal()
    {
        $result = $this->library->create($this->user);
        $this->assertEquals(201, $result["curl"]->infos["http_code"]);
    }

    /**
     * Test token property
     */
    public function tokenProperty($params)
    {
        $this->user->setToken(null);
        $result = $this->library->create($this->user);
        $this->assertEquals(401, $result["curl"]->infos['http_code']);
    }

    /** Test: token */
    public function testTokenProperty()
    {
        // Create account without token
        $this->tokenProperty(null);
        // Create account with a void token
        $this->tokenProperty("");
        // Create account with bad token
        $this->tokenProperty("eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1NDQxMDQxODcsImV4cCI6MTU0NDEwNzc4Nywicm9sZXMiOlsiUk9MRV9VU0VSX0FQUCJdLCJ1c2VybmFtZSI6Im5vcmJlcnRAbXlib29raW5vdS5jb20ifQ.phkdEa0eirz6hQ0SyVb5hifSStemDfT8YLE7KY7joudOp85vgItlFBAtTWdOMTPanJ_yckJcR6yj2M7LEgafyun_jNW94ju3V0V903drd156oVZUYPvFmvI1uwokGTYSyhruSANpsf64_axW6RwfUfom05-adSuowyrVZqWOyPT-s79QPn3botOZkLSpzTTtb0pgtqLOAuKUkUXyUI9ib_qxNlQw0A4fPf12hN49Cemq83MQS6H1wu9ksO2__sptoL5ll_3khjRVlFTc9p5vSYWRR-BYekEiIj9Mwv58LkiEWj1rHbE2NLl6uVTTHk10JRroSay3gL8XHTG8LQ5rJHSBpnIMBM3efeayAA8ulOCYcf9N4R6Z_vnFBNt5dowE9i4zbm8LK--6O_ERmeBeaSBNOXfzU-9Xug7eTEpJpfBIr1p6fkXOpOMWNwesJkliuyT5kcYmOcwG229rCGnEIPKB_6L8aacbSVM6oZYM32NggNzrB0vbMwcs14jMHCS5oIy4x3GBp-Qe5FrYrT8sCgr040y-c83N99zTwYO2tbSIFpVqruUCgMi0pS5QpqeGkMkBiqDDQJPV1mEzpuUgaIX7TYiSiiix-n1_V73nKJ2t8sgg5hB1wjbK1QLCLU52MOFoI7ynTZO1RYVoguXHAhojM-thIX7D32Zyfy4ZthA");
    }

    /**
     * Story property
     */
    public function storyProperty($params)
    {
        $this->library->setStory($params);
        $result = $this->library->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos['http_code']);
    }

    /** Test: story */
    public function testStoryProperty()
    {
        // Create library without story
        $this->storyProperty(null);
        // Create library with wrong story user
        $this->story->setId("999");
        $this->storyProperty($this->story);
        // Create library wiht wrong story format story
        $this->story->setId("WRONG");
        $this->storyProperty($this->story);
    }

    /**
     * Membership property
     */
    public function membershipProperty($params)
    {
        $this->library->setMembership($params);
        $result = $this->library->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos['http_code']);
    }

    /** Test: membership */
    public function testMembershipProperty()
    {
        // Create library without membership user
        $this->membershipProperty(null);
        // Create library wiht wrong membership
        $this->membership->setId("999");
        $this->membershipProperty($this->membership);
        // Create library wiht wrong format
        $this->membership->setId("WRONG");
        $this->membershipProperty($this->membership);
    }

    /**
     * Story property
     */
    public function storyPropertyUpdate($params , $key = 400)
    {
        $this->library->setStory($params);
        Utils::setUpdate($this->library, ["story"]);
        $result = $this->library->update($this->user);
        $this->assertEquals($key, $result["curl"]->infos['http_code']);
    }

    /**Test: Story */
    public function testStoryPropertyUpdate()
    {
        $library = $this->library;

        $library->create($this->user);
        // Update library with a good story data
        $this->storyPropertyUpdate("api/stories/" . $library->story->getId() , 200);
        // Update library without story
        $this->storyPropertyUpdate(null);
        // Update library with wrong story user
        $this->story->setId("999");
        $this->storyPropertyUpdate($this->story);
        // Update library with wrong story format story
        $this->story->setId("WRONG");
        $this->storyPropertyUpdate($this->story);
    }

    /**
     * Update Membership property
     */
    public function membershipPropertyUpdate($params, Library $library, $key = 400)
    {
        $library->setMembership($params);
        Utils::setUpdate($library, ['membership']);
        $result = $library->update($this->user);
        $this->assertEquals($key, $result["curl"]->infos['http_code']);
    }

    /** Test: membership */
    public function testMembershipPropertyUpdate()
    {
        $library = $this->library;
        $library->create($this->user);
        // Update library with a good membership
        $this->membershipPropertyUpdate("api/memberships/" . $this->membership->getId(), $this->library, 200);
        // Update library without membership user
        $this->membershipPropertyUpdate(null, $this->library);
        // Update library with wrong membership
        $this->membership->setId("999");
        $this->membershipPropertyUpdate($this->membership, $this->library);
        // Update library with wrong format
        $this->membership->setId("WRONG");
        $this->membershipPropertyUpdate($this->membership, $this->library);
    }
}