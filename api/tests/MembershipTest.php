<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;

class MembershipTest extends TestCase
{
    /* @var $user User */
    private $user = null;
    /* @var $account Account */
    private $account = null;
    /* @var $membership Membership */
    private $membership = null;
    /* @var $picture MediaObject */
    private $picture = null;

    public function setUp()
    {
        // Step 1: Create user
        $this->user = new User();
        $this->user->create();
        $this->user->login();

        // Step 2: Create user picture
        $mediaObject = new MediaObject();
        $mediaObject->create($this->user);

        // Step 3: Initialize account
        $this->account = new Account();
        $this->account->setPicture($mediaObject);
        $this->account->create($this->user);

        //Step 4 : Initialize membership
        $this->membership = new Membership();
        $this->membership->setAccount($this->account);
    }

    /**
     * Create membership with good data
     */
    public function testNominal()
    {
        $result = $this->membership->create($this->user);

        Utils::varDump($result, "MEMBERSHIP");

        $this->assertEquals(201, $result["curl"]->infos["http_code"]);
    }

    /**
     * Token property
     */
    public function tokenProperty($params)
    {
        $this->user->setToken($params);
        $result = $this->account->create($this->user);
        $this->assertEquals(401, $result["curl"]->infos['http_code']);
    }

    /** Test: token */
    public function testTokenProperty()
    {
        // Create account without token
        $this->tokenProperty(null);
        // Create account with empty token
        $this->tokenProperty('');
        // Create account with bad token
        $this->tokenProperty("eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1NDQxMDQxODcsImV4cCI6MTU0NDEwNzc4Nywicm9sZXMiOlsiUk9MRV9VU0VSX0FQUCJdLCJ1c2VybmFtZSI6Im5vcmJlcnRAbXlib29raW5vdS5jb20ifQ.phkdEa0eirz6hQ0SyVb5hifSStemDfT8YLE7KY7joudOp85vgItlFBAtTWdOMTPanJ_yckJcR6yj2M7LEgafyun_jNW94ju3V0V903drd156oVZUYPvFmvI1uwokGTYSyhruSANpsf64_axW6RwfUfom05-adSuowyrVZqWOyPT-s79QPn3botOZkLSpzTTtb0pgtqLOAuKUkUXyUI9ib_qxNlQw0A4fPf12hN49Cemq83MQS6H1wu9ksO2__sptoL5ll_3khjRVlFTc9p5vSYWRR-BYekEiIj9Mwv58LkiEWj1rHbE2NLl6uVTTHk10JRroSay3gL8XHTG8LQ5rJHSBpnIMBM3efeayAA8ulOCYcf9N4R6Z_vnFBNt5dowE9i4zbm8LK--6O_ERmeBeaSBNOXfzU-9Xug7eTEpJpfBIr1p6fkXOpOMWNwesJkliuyT5kcYmOcwG229rCGnEIPKB_6L8aacbSVM6oZYM32NggNzrB0vbMwcs14jMHCS5oIy4x3GBp-Qe5FrYrT8sCgr040y-c83N99zTwYO2tbSIFpVqruUCgMi0pS5QpqeGkMkBiqDDQJPV1mEzpuUgaIX7TYiSiiix-n1_V73nKJ2t8sgg5hB1wjbK1QLCLU52MOFoI7ynTZO1RYVoguXHAhojM-thIX7D32Zyfy4ZthA");
    }

    /**
     * Test with Role property
     */
    public function roleProperty($params)
    {
        $result = $this->membership->setRole($params)->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos["http_code"]);
    }

    /**Test: role */
    public function testRoleProperty()
    {
        // Create Membership without role
        $this->roleProperty(null);
        // Create Membership with empty role
        $this->roleProperty("");
        // Create Membership with a role too long
        $this->roleProperty("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        // Create with a role too short
        $this->roleProperty("1");
    }

    /**
     * Pseudo property
     */
    public function pseudoProperty($params , $key)
    {
        $result = $this->membership->setPseudo($params)->create($this->user);
        $this->assertEquals($key, $result["curl"]->infos["http_code"]);
    }

    /**Test: pseudo */
    public function testPseudoProperty()
    {
        // Create Membership without pseudo
        $this->pseudoProperty(null, 201);
        // Create Membership with a pseudo too long
        $this->pseudoProperty("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", 400);
        // Create Membership with a void role
        $this->pseudoProperty("", 201);
        // Create Membership with a too short role
        $this->pseudoProperty("12", 400);
    }

    /**
     * Update Pseudo property
     */
    public function pseudoPropertyUpdate($params, Membership $membership, $key = 400)
    {
        $membership->setPseudo($params);
        Utils::setUpdate($membership, ['pseudo']);
        $result = $membership->update($this->user);
        $this->assertEquals($key, $result["curl"]->infos["http_code"]);
    }

    /** Test: pseudo */
    public function testPseudoPropertyUpdate()
    {
        // Create membership
        $membership = $this->membership;
        $membership->create($this->user);
        // Update Membership role with good data
        $this->pseudoPropertyUpdate("papa", $this->membership, 200);
        // Update Membership without role
        $this->pseudoPropertyUpdate(null, $this->membership);
        // Update Membership with a role too long
        $this->pseudoPropertyUpdate('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', $this->membership);
        // Update Membership with a too short pseudo
        $this->pseudoPropertyUpdate("1", $this->membership);
    }

    /**
     * Update Role property
     */
    public function updateRoleProperty($params, Membership $membership, $key = 400)
    {
        $membership->setRole($params);
        Utils::setUpdate($membership, ["role"]);
        $result = $membership->update($this->user);
        $this->assertEquals($key, $result["curl"]->infos["http_code"]);
    }

    /** Test: role */
    public function testUpdateRoleProperty()
    {
        // Create membership
        $membership = $this->membership;
        $membership->create($this->user);
        // Update membership with good data role
        $this->updateRoleProperty("papa", $membership, 200);
        // Update membership without role
        $this->updateRoleProperty(null, $membership);
        // Update membership with a too long role
        $this->updateRoleProperty("PPPPPPPPPPPPPAAAAAAAAAAAAAAAAAAAAAAAPPPPPPPPPPPPPPPPPPPPPPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", $membership);
        // Update membership with a too short role
        $this->updateRoleProperty("1", $membership);
    }


    /////////////////////////// TEST ACCOUNT PROPERTIES ///////////////////////////////////////

    /**
     * Test with Account property
     */
    public function testAccountProperty()
    {
        // test uidDevice
        $result = $this->account->setUidDevice(null)->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos['http_code']);
        $result = $this->account->setUidDevice("")->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos['http_code']);

        // Create account without picture
        $this->account->setPicture(null);
        $result = $this->account->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos['http_code']);

        // Create account with bad uri picture
        $this->picture = new MediaObject();
        $this->picture->setId("999");
        $this->account->setPicture($this->picture);
        $result = $this->account->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos['http_code']);

        // Create account with bad uri picture format
        $this->picture->setFile("qsgffgsdfgsdf");
        $this->account->setPicture($this->picture);
        $result = $this->account->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos['http_code']);

        // Create account without name
        $result = $this->account->setName(null)->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos['http_code']);
        $result = $this->account->setName("")->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos['http_code']);

        // Create account with a name too short
        $result = $this->account->setName("a")->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos['http_code']);

        // Create account with a name too long
        $result = $this->account->setName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos['http_code']);
    }
}