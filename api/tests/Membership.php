<?php

namespace App\Tests;

class Membership
{
    public $id = null;
    public $role = null;
    public $pseudo = null;
    /* @var $account Account*/
    public $account = null;
    public $rawProperties = null;
    public $isAdmin = null;

    /**
     * Membership constructor.
     * @param $role
     * @param $pseudo
     */
    function __construct($role = "Papa", $pseudo = "Pseudo")
    {
        $this->role = $role;
        $this->pseudo = $pseudo;
        $this->isAdmin = "true";
    }

    // Add membership to API
    public function create(User $user, $memberCreate = false)
    {
        $this->account->getPicture() !== null ? $pictureAccountId = "api/media_objects/" . $this->account->getPicture()->getId() : $pictureAccountId = null;

        if ($memberCreate === false) {
        $parameters = sprintf('{"account": {"name": "%s", "picture": "%s", "uidDevice": "%s", "code": "%s"}, "role": "%s", "pseudo": "%s", "isAdmin": ' . "%s" . '}',
            $this->getAccount()->getName(),
            $pictureAccountId,
            $this->getAccount()->getUidDevice(),
            $this->getAccount()->getCode(),
            $this->getRole(),
            $this->getPseudo(),
            $this->getIsAdmin());

            } else {
            $parameters = sprintf('{"account": "%s", "role": "%s", "pseudo": "%s"}',
               "api/accounts/" . $this->getAccount()->getId(), $this->getRole(), $this->getPseudo()
            );
        }

        $lazyCurl = new LazyCurl("memberships", $user->getToken(), $parameters);
        return $lazyCurl->execute($this);
    }

    public function update(User $user)
    {
        if ($this->id !== null) {
            $lazyCurl = new LazyCurl("memberships/" . $this->id, $user->getToken());
            $lazyCurl->setType('PUT')->setParameters($this->rawProperties);
            $result = $lazyCurl->execute();
            return $result;
        }
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id; return $this;
    }

    public function getRole() {
        return $this->role;
    }

    public function setRole($role) {
        $this->role = $role; return $this;
    }

    public function getPseudo() {
        return $this->pseudo;
    }

    public function setPseudo($pseudo) {
        $this->pseudo = $pseudo; return $this;
    }

    public function getAccount(): Account {
        return $this->account;
    }

    public function setAccount(Account $account) {
        $this->account = $account; return $this;
    }

    public function getRawProperties() {
        return $this->rawProperties;
    }

    public function setRawProperties($rawProperties) {
        $this->rawProperties = $rawProperties; return $this;
    }

    public function getIsAdmin() {
        return $this->isAdmin;
    }

    public function setIsAdmin( $isAdmin) {
        $this->isAdmin = $isAdmin; return $this;
    }
}