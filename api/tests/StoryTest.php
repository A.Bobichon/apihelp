<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;

class StoryTest extends  TestCase {
    /* @var $user User */
    private $user = null;
    /* @var $story Story */
    private $story = null;
    /* @var $sound MediaObject */
    private $sound = null;

    public function setUp() {
        // Step 1: Create user
        $this->user = new User();
        $this->user->create();
        $this->user->login();

        // Step 2: Create story picture
        $picture = new MediaObject();
        $picture->create($this->user);

        // Step 3: Create story sound
        $sound = new MediaObject();
        $sound->setFile("sounds/04E4CA0A542880.mp3");
        $sound->create($this->user);

        // Step 3: Initialize story
        $this->story = new Story();
        $this->story->setPicture($picture);
        $this->story->setSound($sound);
    }

    /**
     * Create story with good data
     */
    public function testNominal() {
        $result = $this->story->create($this->user);
        $this->assertEquals(201, $result["curl"]->infos['http_code']);
    }

    /**
     * Token property
     */
    public function tokenProperty($params) {
        // Create story without token
        $this->user->setToken($params);
        $result = $this->story->create($this->user);
        $this->assertEquals(401, $result["curl"]->infos['http_code']);
    }

    /** Test: token */
    public function testTokenProperty() {
        // Create story without token
        $this->tokenProperty(null);
        // Create with a void token
        $this->tokenProperty("");
        // Create story with bad token
        $this->tokenProperty("eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1NDQxMDQxODcsImV4cCI6MTU0NDEwNzc4Nywicm9sZXMiOlsiUk9MRV9VU0VSX0FQUCJdLCJ1c2VybmFtZSI6Im5vcmJlcnRAbXlib29raW5vdS5jb20ifQ.phkdEa0eirz6hQ0SyVb5hifSStemDfT8YLE7KY7joudOp85vgItlFBAtTWdOMTPanJ_yckJcR6yj2M7LEgafyun_jNW94ju3V0V903drd156oVZUYPvFmvI1uwokGTYSyhruSANpsf64_axW6RwfUfom05-adSuowyrVZqWOyPT-s79QPn3botOZkLSpzTTtb0pgtqLOAuKUkUXyUI9ib_qxNlQw0A4fPf12hN49Cemq83MQS6H1wu9ksO2__sptoL5ll_3khjRVlFTc9p5vSYWRR-BYekEiIj9Mwv58LkiEWj1rHbE2NLl6uVTTHk10JRroSay3gL8XHTG8LQ5rJHSBpnIMBM3efeayAA8ulOCYcf9N4R6Z_vnFBNt5dowE9i4zbm8LK--6O_ERmeBeaSBNOXfzU-9Xug7eTEpJpfBIr1p6fkXOpOMWNwesJkliuyT5kcYmOcwG229rCGnEIPKB_6L8aacbSVM6oZYM32NggNzrB0vbMwcs14jMHCS5oIy4x3GBp-Qe5FrYrT8sCgr040y-c83N99zTwYO2tbSIFpVqruUCgMi0pS5QpqeGkMkBiqDDQJPV1mEzpuUgaIX7TYiSiiix-n1_V73nKJ2t8sgg5hB1wjbK1QLCLU52MOFoI7ynTZO1RYVoguXHAhojM-thIX7D32Zyfy4ZthA");
    }

    /**
     * Title property
     */
    public function titleProperty($param) {
        // Create story without title
        $result = $this->story->setTitle($param)->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos['http_code']);
    }

    /** Test: title */
    public function testTitleProperty() {
        // Create story without title
        $this->titleProperty(null);
        // Create story with a void title
        $this->titleProperty("");
        // Create story with too short title
        $this->titleProperty("a");
        // Create story with too long title
       $this->titleProperty("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    }

    /**
     * Picture property
     */
    public function pictureProperty($params) {
        // Create story without picture
        $this->story->setPicture($params);
        $result = $this->story->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos['http_code']);
    }

    /** Test: picture */
    public function testPictureProperty() {
        $mediaObject = $this->story->getPicture();
        // Create story without picture
        $this->pictureProperty(null);
        // Create story with bad uri picture
        $mediaObject->setId("999");
        $this->pictureProperty($mediaObject);
        // Create story with bad uri picture format
        $mediaObject->setFile("qsgffgsdfgsdf");
       $this->pictureProperty($mediaObject);
    }

    /**
     * Sound property
     */
    public function soundProperty($params) {
        // Create story without sound
        $this->story->setSound($params);
        $result = $this->story->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos['http_code']);
    }

    /** Test: sound */
    public function testSoundProperty() {
        $mediaObject = $this->story->getPicture();
        // Create story without sound
        $this->soundProperty(null);
        // Create story with bad uri sound
        $mediaObject->setId("999");
        $this->soundProperty($mediaObject);
        // Create story with bad uri sound format
        $mediaObject->setFile("qsgffgsdfgsdf");
        $this->soundProperty($mediaObject);
    }

    /**
     * Duration property
     */
    public function durationProperty($params) {
        // Create story without duration
        $result = $this->story->setDuration($params)->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos['http_code']);
    }

    /** Test: duration */
    public function testDurationProperty() {
        // Create story without duration
        $this->durationProperty(null);
        // Create story with a void duration
        $this->durationProperty("");
        // Create story with bad duration format
        $this->durationProperty("100:02:31");
        // Create story with negative duration
        $this->durationProperty("00:-02:31");
    }

    /**
     * Update Title
     */
    public function titlePropertyUpdate($params, $key = 400){
        // Update story with a good title
        $this->story->setTitle($params);
        Utils::setUpdate($this->story, ['title']);
        $result = $this->story->update($this->user);
        $this->AssertEquals($key, $result["curl"]->infos['http_code']);
    }

    /** Test: title */
    public function testTitlePropertyUpdate(){
        // Create story for test update
        $story = $this->story;
        $story->create($this->user);
        // Update story with a good title
        $this->titlePropertyUpdate("LeBonTitre" ,200);
        // Update story without a title
        $this->titlePropertyUpdate(null);
        // Update story with title too long
        $this->titlePropertyUpdate("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
        // Update story empty title
        $this->titlePropertyUpdate("");
    }

    /**
     * Test update Picture
     */
     public function testPicturePropertyUpdate(){
         // Create story for update tests
         $story=$this->story;
         $story->create($this->user);
         $mediaObject = $this->story->getPicture();

         // Update story with a good picture
         $story->setPicture("api/media_objects/" . $story->getPicture()->id);
         Utils::setUpdate($story, ['picture']);
         $result = $story->update($this->user);
         $this->assertEquals(200, $result["curl"]->infos['http_code']);

         // Update story without picture
         $story->setPicture(null);
         Utils::setUpdate($story, ['picture']);
         $result = $story->update($this->user);
         $this->assertEquals(400, $result["curl"]->infos['http_code']);

         // Update story with bad picture uri
         $story->setSound($mediaObject);
         $story->getSound()->setId('999');
         Utils::setUpdate($story, ['picture']);
         $result = $story->update($this->user);
         $this->assertEquals(400, $result["curl"]->infos['http_code']);

         // Update story with bad uri picture format
         $story->setSound($mediaObject);
         $story->getSound()->setId("WRONG");
         Utils::setUpdate($story, ['picture']);
         $result = $story->update($this->user);
         $this->assertEquals(400, $result["curl"]->infos['http_code']);
     }

    /**
     * Update Sound
     */
    public function soundPropertyUpdate($params, $key = 400){
        // Update story with good data sound
        $this->story->setSound($params);
        Utils::setUpdate($this->story, ['sound']);
        $result = $this->story->update($this->user);
        $this->assertEquals($key, $result["curl"]->infos['http_code']);
    }

    /** Test: sound */
    public function testSoundPropertyUpdate(){
        // Set up test
        $story = $this->story;
        $story->create($this->user);
        $mediaObject = $story->getSound();
        // Update story with good data sound
        $this->soundPropertyUpdate("api/media_objects/" . $mediaObject->getId(), 200);
        // Update story without sound
        $this->soundPropertyUpdate(null);
        // Update story with bad sound uri
        $mediaObject->setId("999");
        $this->soundPropertyUpdate($mediaObject);
        // Update story with bad sound format uri
        $this->soundPropertyUpdate("qsgffgsdfgsdf");
    }

    /**
     * Test Update Duration
     */
    public function durationPropertyUpdate($params, $key = 400) {
        // Update story with a hood data duration
        $this->story->setDuration($params);
        Utils::setUpdate($this->story, ['duration']);
        $result = $this->story->update($this->user);
        $this->assertEquals($key, $result["curl"]->infos['http_code']);
    }

    /** Test: duration */
    public function testDurationPropertyUpdate() {
        $story=$this->story;
        $story->create($this->user);

        // Update story with a hood data duration
        $this->durationPropertyUpdate("00:02:31", 200);
        // Update story without duration
        $this->durationPropertyUpdate(null);
        // Update story with empty duration
        $this->durationPropertyUpdate("");
        // Update story with bad duration format
        $this->durationPropertyUpdate("WRONG");
        // Update story with negative duration
        $this->durationPropertyUpdate("00:-02:31");
    }
}
