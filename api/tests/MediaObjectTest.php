<?php

namespace App\Tests;

use League\Flysystem\Util;
use PHPUnit\Framework\TestCase;

class MediaObjectTest extends TestCase {
    /* @var $user User */
    private $user = null;
    /* @var $mediaObject MediaObject */
    private $mediaObject = null;

    public function setUp() {
        // Step 1: Create user
        $this->user = new User();
        $this->user->create();
        $this->user->login();

        // Step 2: Initialize mediaObject
        $this->mediaObject = new MediaObject();
    }

    /**
     * Update isActive property
     */
    public function updateIsActiveProperty($paramms,User $user,MediaObject $mediaObject ,$key = 400){
        $mediaObject->setIsActive($paramms);
        Utils::setUpdate($mediaObject, ["isActive"]);
        $result = $mediaObject->update($user);
        $this->assertEquals($key, $result["curl"]->infos['http_code']);
    }

    /**
     * Update isActive property
     */
    public function testUpdateIsActiveProperty(){
        // Update is Active with no params
        $this->updateIsActiveProperty(null,$this->user,$this->mediaObject, 200);
    }
}