<?php

namespace App\Tests;

use League\Flysystem\Util;
use PHPUnit\Framework\TestCase;

class AccountTest extends TestCase {
    /* @var $user User */
    private $user = null;
    /* @var $account Account */
    private $account = null;
    /* @var $membership Membership */
    private $membership = null;
    /* @var $mediaObject MediaObject */
    private $mediaObject = null;

    public function setUp() {
        // Step 1: Create user
        $this->user = new User();
        $this->user->create();
        $this->user->login();

        // Step 2: Create user picture
        $mediaObject = new MediaObject();
        $mediaObject->create($this->user);

        // Step 3: Initialize account
        $this->account = new Account();
        $this->account->setPicture($mediaObject);
    }

    /**
     * Create account with good data
     */
    public function testNominal() {
        $result = $this->account->create($this->user);
        $this->assertEquals(201, $result["curl"]->infos['http_code']);
    }

    /**
     * Test tooken property
     */
    public function tookenProperty($param) {
        $this->user->setToken($param);
        $result = $this->account->create($this->user);
        $this->assertEquals(401, $result["curl"]->infos['http_code']);
    }

    /** Token property */
    public function testTookenProperty() {
        // Create account without tooken
        $this->tookenProperty(null);
        // Create account with a void token
        $this->tookenProperty("");
        // Create account with bad tooken
        $this->tookenProperty("eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1NDQxMDQxODcsImV4cCI6MTU0NDEwNzc4Nywicm9sZXMiOlsiUk9MRV9VU0VSX0FQUCJdLCJ1c2VybmFtZSI6Im5vcmJlcnRAbXlib29raW5vdS5jb20ifQ.phkdEa0eirz6hQ0SyVb5hifSStemDfT8YLE7KY7joudOp85vgItlFBAtTWdOMTPanJ_yckJcR6yj2M7LEgafyun_jNW94ju3V0V903drd156oVZUYPvFmvI1uwokGTYSyhruSANpsf64_axW6RwfUfom05-adSuowyrVZqWOyPT-s79QPn3botooZkLSpzTTtb0pgtqLOAuKUkUXyUI9ib_qxNlQw0A4fPf12hN49Cemq83MQS6H1wu9ksO2__sptooL5ll_3khjRVlFTc9p5vSYWRR-BYekEiIj9Mwv58LkiEWj1rHbE2NLl6uVTTHk10JRroSay3gL8XHTG8LQ5rJHSBpnIMBM3efeayAA8ulOCYcf9N4R6Z_vnFBNt5dowE9i4zbm8LK--6O_ERmeBeaSBNOXfzU-9Xug7eTEpJpfBIr1p6fkXOpOMWNwesJkliuyT5kcYmOcwG229rCGnEIPKB_6L8aacbSVM6oZYM32NggNzrB0vbMwcs14jMHCS5oIy4x3GBp-Qe5FrYrT8sCgr040y-c83N99zTwYO2tbSIFpVqruUCgMi0pS5QpqeGkMkBiqDDQJPV1mEzpuUgaIX7TYiSiiix-n1_V73nKJ2t8sgg5hB1wjbK1QLCLU52MOFoI7ynTZO1RYVoguXHAhojM-thIX7D32Zyfy4ZthA");
    }


    /**
     * Test name property
     */
    public function nameProperty($params) {
        $result = $this->account->setName($params)->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos['http_code']);
    }

    /** Test: name */
    public function testNameProperty() {
        // Create account without name
        $this->nameProperty(null);
        // Create account with a empty name
        $this->nameProperty("");
        // Create account with too short name
        $this->nameProperty("1");
        // Create account with too long name
        $this->nameProperty("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
    }

    /**
     * Test picture property
     */
    public function pictureProperty($params) {
        $this->account->setPicture($params);
        $result = $this->account->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos['http_code']);
    }

    /** Test : picture */
    public function testPictureProperty() {
        $mediaObject = $this->account->getPicture();
        // Create account without picture
        $this->pictureProperty(null);
        // Create account with bad uri picture
        $mediaObject->setId("999");
        $this->pictureProperty($mediaObject);

        // Create account with bad uri picture format
        $mediaObject->setFile("qsgffgsdfgsdf");
        $this->pictureProperty($mediaObject);
    }

    /**
     * Test uid device property
     */
    public function testUidDeviceProperty() {
        // Create account without uid device
        $result = $this->account->setUidDevice(null)->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos['http_code']);
        // Create account with a void uiDevice
        $result = $this->account->setUidDevice("")->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos['http_code']);
    }

    /**
     * Test code property
     */
    public function codeProperty($params) {
        $result = $this->account->setCode($params)->create($this->user);
        $this->assertEquals(201, $result["curl"]->infos['http_code']);

    }

    /** Test: code */
    public function testCodeProperty()
    {
        // Create account without code
        $this->codeProperty(null);
        // Create account with a void code
        $this->codeProperty("");
        // Create account with too short code
        $this->codeProperty("0");
        // Create account with too long code
        $this->codeProperty("000000000000000000000000000000000000000");
    }

    /**
     * Update Name property
     */
    public function updateNameProperty($params,Account $account, $key = 400){
        $account->setName($params);
        Utils::setUpdate($account, ["name"]);
        $result = $account->update($this->user);
        $this->assertEquals($key, $result["curl"]->infos["http_code"]);
    }

    /** Test: name */
    public function testUpdateNameProperty() {
        // Set up Account for update
        $this->membership = new Membership();
        $this->membership->setAccount($this->account);
        $result = $this->membership->create($this->user);

        $accountResult = json_decode($result["data"])->{'account'};
        $explode = explode("/", $accountResult->{"@id"});
        $accountId =  $explode[count($explode) - 1];
        $account = new Account();
        $account->setId($accountId);

        // Update name with good data
        $this->updateNameProperty("LE TEST" ,$account, 200);

        // Update name with no data
        $this->updateNameProperty(null ,$account);
        // Update name with a name too short
        $this->updateNameProperty("1" ,$account);
        // Update name with a empty value
        $this->updateNameProperty("" ,$account);
        // Update name with a too long value
        $this->updateNameProperty("LE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TEST LE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TESTLE TEST" , $account);
    }

    /**
     * Update picture properties
     */
    public function pictureUpdateProperty($params, Account $account, $key = 400){
        // Update with good data
        $account->setPicture($params);
        Utils::setUpdate($account, ["picture"]);
        $result = $account->update($this->user);
        $this->assertEquals($key, $result["curl"]->infos["http_code"]);
    }

    /** Test: picture */
    public function testPictureUpdateProperty(){
        // Set up Account for update
        $this->membership = new Membership();
        $this->membership->setAccount($this->account);
        $result = $this->membership->create($this->user);

        $accountResult = json_decode($result["data"])->{'account'};
        $explode = explode("/", $accountResult->{"@id"});
        $accountId =  $explode[count($explode) - 1];
        $account = new Account();
        $account->setId($accountId);
        $media = new MediaObject();
        $media->create($this->user);
        $account->setPicture($media);

        // Update with good data
        $this->pictureUpdateProperty("api/media_objects/" . $media->getId(), $account, 200);
        // Update picture with wrong media
        $media->create($this->user);
        $account->setPicture($media);
        $data = $account->getPicture()->setId("999");
        $this->pictureUpdateProperty( $data, $account);
        // Update picture with wrong format
        $data = $account->getPicture()->setFile("WRONG");
        $this->pictureUpdateProperty($data, $account);
        // Update picture with good data null
        $data = $account->setPicture(null);
        $this->pictureUpdateProperty($data, $account);
    }

    /**
     * Test Update uidDevice property
     */
    public function updateUiDeviceProperty($params, Account $account, $key = 400){
        // Update uidDevice with good data
        $account->setUidDevice($params);
        Utils::setUpdate($account, ["uidDevice"]);
        $result = $account->update($this->user);
        $this->assertEquals($key, $result["curl"]->infos["http_code"]);
    }

    /** Test: uiDevice */
    public function testUpdateUiDeviceProperty(){
        // Set up Account for update
        $this->membership = new Membership();
        $this->membership->setAccount($this->account);
        $result = $this->membership->create($this->user);

        $accountResult = json_decode($result["data"])->{'account'};
        $explode = explode("/", $accountResult->{"@id"});
        $accountId =  $explode[count($explode) - 1];
        $account = new Account();
        $account->setId($accountId);

        // Update uidDevice with good data
        $this->updateUiDeviceProperty("00003", $account, 200);
        // Update uiDevice with empty data
        $this->updateUiDeviceProperty("", $account);
        // Update uiDevice with data null
        $this->updateUiDeviceProperty(null, $account);
    }

    /**
     * Update code property
     */
    public function codeUpdateProperty($params ,Account $account ,$key = 200){
        // Update code with good data
        $account->setCode($params);
        Utils::setUpdate($account, ["code"]);
        $result = $account->update($this->user);
        $this->assertEquals($key, $result["curl"]->infos["http_code"]);
    }

    /** Test: code */
    public function testCodeUpdateProperty(){
        // Set up Account for update
        $this->membership = new Membership();
        $this->membership->setAccount($this->account);
        $result = $this->membership->create($this->user);

        $accountResult = json_decode($result["data"])->{'account'};
        $explode = explode("/", $accountResult->{"@id"});
        $accountId =  $explode[count($explode) - 1];
        $account = new Account();
        $account->setId($accountId);

        // Update code with good data
        $this->codeUpdateProperty("0000000", $account);
        // Update code with data null
        $this->codeUpdateProperty(null, $account, 400);
        // Update code with empty data
        $this->codeUpdateProperty("", $account);
        // Update code with a too short code
        $this->codeUpdateProperty("0", $account);
        // Update code with a too long code
        $this->codeUpdateProperty("0000000000000000000000000000", $account);
    }
}