<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 09/12/18
 * Time: 19:21
 */

namespace App\Tests;

class Story {
    public $id = null;
    public $title = null;
    /* @var $picture MediaObject */
    public $picture = null;
    /* @var $sound MediaObject */
    public $sound = null;
    public $duration = null;
    /* @var $library Library */
    public $library = null;
    public $isActive = null;
    public $rawProperties = null;

    public function __construct($title = "Léo chez le docteur", $duration = "00:02:31") {
        $this->title = $title;
        $this->duration = $duration;
        $this->isActive = true;
    }

    // Add story to API
    public function create(User $user) {
        $this->getPicture() !== null ? $pictureId = "api/media_objects/" . $this->getPicture()->getId() : $pictureId = null;
        $this->getSound() !== null ? $soundId = "api/media_objects/" . $this->getSound()->getId() : $soundId = null;
        $parameters = sprintf('{"title": "%s", "picture": "%s", "sound": "%s", "duration": "%s"}',
            $this->getTitle(),
            $pictureId,
            $soundId,
            $this->getDuration()
        );

        $lazyCurl = new LazyCurl("stories", $user->getToken(), $parameters);
        return $lazyCurl->execute($this);
    }

    // Update Story
    public function update(User $user){
        if($this->id !== null){
            $lazyCurl = new LazyCurl('stories/' . $this->id, $user->token);
            $lazyCurl->setType('PUT')->setParameters($this->rawProperties);
            $result = $lazyCurl->execute();
            return $result;
        }
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id; return $this;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title; return $this;
    }

    public function getPicture() {
        return $this->picture;
    }

    public function setPicture($picture) {
        $this->picture = $picture; return $this;
    }

    public function getSound() {
        return $this->sound;
    }

    public function setSound($sound) {
        $this->sound = $sound; return $this;
    }

    public function getDuration() {
        return $this->duration;
    }

    public function setDuration($duration) {
        $this->duration = $duration; return $this;
    }

    public function getLibrary(){
        return $this->library;
    }

    public function setIsActive($isActive) {
        $this->isActive = $isActive; return $this;
    }

    public function getIsActive(){
        return $this->isActive;
    }

    public function setLibrary($library){
        $this->library = $library; return $this;
    }

    public function getRawProperties(){
        $this->rawProperties;
    }

    public function setRawProperties($rawProperties){
        $this->rawProperties = $rawProperties;
    }
}