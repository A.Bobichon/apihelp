<?php

namespace App\Tests;

class Tag {

    public $id = null;
    public $uid = null;
    /* @var $story Story */
    public $story = null;
    /* @var $user User */
    public $user = null;
    public $rawProperties = null;
    public $isActive = null;

    /**
     * Tag constructor.
     * @param $uid
     */
    function __construct($uid = "Aventure"){
        $this->uid = $uid;
        $this->isActive = "true";
    }

    // Add tag to API
    public function create(User $user){
        $this->getStory() !== null ? $storyId = "api/stories/" . $this->getStory()->getId() : $storyId = null;
        $this->getOwner() !== null ? $ownerId = "api/users/" . $this->getOwner()->getId() : $ownerId = null;
        $parameters = sprintf('{"uid": "%s", "story": "%s" , "owner": "%s"}',
           $this->getUid(), $storyId , $ownerId);

        $lazyCurl = new LazyCurl("tags", $user->getToken(), $parameters);
        return $lazyCurl->execute($this);
    }

    // Update tag
    public function update(User $user){
        if($this->id !==null){
            $lazyCurl = new LazyCurl('tags/' . $this->id, $user->token);
            $lazyCurl->setType('PUT')->setParameters($this->rawProperties);
            $result = $lazyCurl->execute();
            return $result;
        }
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id; return $this;
    }

    public function getUid() {
        return $this->uid;
    }

    public function setUid($uid) {
        $this->uid = $uid; return $this;
    }

    public function getStory() {
        return $this->story;
    }

    public function setStory( $story) {
        $this->story = $story; return $this;
    }

    public function getOwner() {
        return $this->user;
    }

    public function setOwner($user) {
        $this->user = $user; return $this;
    }

    public function getIsActive() {
        return $this->isActive;
    }

    public function setIsActive($isActive) {
        $this->isActive = $isActive; return $this;
    }

    public function getRawProperties(){
        $this->rawProperties;
    }

    public function setRawProperties($rawProperties){
        $this->rawProperties = $rawProperties;
    }
}
