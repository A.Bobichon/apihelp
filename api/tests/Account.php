<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 09/12/18
 * Time: 13:47
 */

namespace App\Tests;

use App\Exception\NotFoundException;

/**
 * Class Account
 * @package App\Tests
 */
class Account {
    public $id = null;
    public $name = null;
    /* @var $picture MediaObject */
    public $picture = null;
    public $uidDevice = null;
    public $code = null;
    public $rawProperties = null;
    /* @var $membership Membership */
    public $membership = null;

    /**
     * Account constructor.
     * @param string $name
     * @param string $uidDevice
     * @param string $code
     */
    public function __construct($name = "Le compte d'alexandre", $uidDevice = "00003", $code = "0000000") {
        $this->name = $name;
        $this->uidDevice = $uidDevice;
        $this->code = $code;
    }

    // Add account to API
    public function create(User $user) {
        $this->getPicture() !== null ? $pictureId = "api/media_objects/" . $this->getPicture()->getId() : $pictureId = null;
        $parameters = sprintf('{"name": "%s", "picture": "%s", "uidDevice": "%s", "code": "%s"}',
            $this->getName(), $pictureId, $this->getUidDevice(), $this->getCode());

        $lazyCurl = new LazyCurl("accounts", $user->getToken(), $parameters);
        return $lazyCurl->execute($this);
    }

    public function update(User $user)
    {
        if ($this->id !== null) {
            $lazyCurl = new LazyCurl("accounts/" . $this->id, $user->getToken());
            $lazyCurl->setType('PUT')->setParameters($this->rawProperties);
            $result = $lazyCurl->execute();
            return $result;
        }
        throw new NotFoundException("ERROR");
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id; return $this;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name; return $this;
    }

    public function getPicture() {
        return $this->picture;
    }

    public function setPicture($picture) {
        $this->picture = $picture; return $this;
    }

    public function getUidDevice() {
        return $this->uidDevice;
    }

    public function setUidDevice($uidDevice) {
        $this->uidDevice = $uidDevice; return $this;
    }

    public function getCode() {
        return $this->code;
    }

    public function setCode($code) {
        $this->code = $code; return $this;
    }

    public function getMembership(){
        return $this->membership;
    }

    public function setMembership($membership){
        $this->membership = $membership;
    }

    public function getRawProperties() {
        return $this->rawProperties;
    }

    public function setRawProperties($rawProperties) {
        $this->rawProperties = $rawProperties; return $this;
    }
}