<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 09/12/18
 * Time: 14:09
 */

namespace App\Tests;


use App\Exception\NotFoundException;
use League\Flysystem\Util;

class User {
    public $token = null;
    public $id = null;
    public $username = null;
    public $password = null;
    public $firstName = null;
    public $lastName = null;
    public $rawProperties = null;
    public $newPassword = null;
    // Check is method setUsername is called
    public $methodCall = true;

    public function __construct($password = "alex1234", $firstName = "Alexandre", $lastName = "Bobichon") {
        // Generate a random email
        $this->username = null;
        $this->password = $password;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    // Add user to API
    public function create() {
        // Set parameters
        if ($this->methodCall === true) {
            $username = Utils::generateRandomValue();
            $data = $username . "@mybookinou.com";
            $this->setUsername($data);
        }
        $parameters = sprintf('{"username": "%s", "password": "%s", "firstName": "%s", "lastName": "%s"}',
            $this->username, $this->password, $this->firstName, $this->lastName);

        $lazyCurl = new LazyCurl('users');
        $lazyCurl->setParameters($parameters);
        return $lazyCurl->execute($this);
    }

    // Update user to API
    public function update() {
        if ($this->id !== null) {
            $lazyCurl = new LazyCurl("users/" . $this->id, $this->token);
            $lazyCurl->setType('PUT')->setParameters($this->rawProperties);
            $result = $lazyCurl->execute();
            return $result;
        }
        throw new NotFoundException("Can't update entity without id");
    }

    // Login user to API
    public function login() {
        $parameters = sprintf('{"username": "%s", "password": "%s"}',
            $this->getUsername(),
            $this->getPassword());

        $lazyCurl = new LazyCurl('login');
        $lazyCurl->setParameters($parameters);
        $result = $lazyCurl->execute();

        $this->token = json_decode($result["data"])->{'token'};

        Utils::loadUser($this);

        return array("data" => $result, "curl" => $lazyCurl);
    }

    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id; return $this;
    }

    public function getToken() {
        return $this->token;
    }

    public function setToken($token) {
        $this->token = $token; return $this;
    }

    public function getUsername() {
        return $this->username;
    }

    public function setUsername($username) {
        $this->methodCall = false;
        $this->username = $username; return $this;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password; return $this;
    }

    public function getFirstName() {
        return $this->firstName;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName; return $this;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName; return $this;
    }

    public function getRawProperties() {
        return $this->rawProperties;
    }

    public function setRawProperties($rawProperties) {
        $this->rawProperties = $rawProperties;
    }

    public function getNewPassword() {
        return $this->newPassword;
    }

    public function setNewPassword($newPassword) {
        $this->newPassword = $newPassword;
    }
}