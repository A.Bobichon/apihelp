<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 29/11/18
 * Time: 23:22
 */

namespace App\Tests;

class LazyCurl {
    private $path = 'https://api-v0.mybookinou.com/api/';
    //private $path = 'http://localhost/bookinou/api/public/api/';
    private $type = 'POST';
    private $contentType = 'application/ld+json';
    private $authorization = '';
    private $parameters = '';
    public $infos = '';
    private $result = '';

    public function __construct($url = null, $token = null, $parameters = null) {
        $this->authorization = 'Bearer ' . $token;
        $this->parameters = $parameters;
        $this->path .= $url;
    }

    /**
     * Execute curl query. Auto set entity id
     * @param null $entity
     * @return array
     */
    public function execute($entity = null) {
        $ch = curl_init($this->path);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: ' . $this->getContentType(), 'Authorization: ' . $this->authorization));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->type);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->parameters);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $this->result = curl_exec($ch);

        $this->infos = curl_getinfo($ch);
        curl_close($ch);

        if ($this->infos['http_code'] === 201 && $entity !== null) $entity->setId(Utils::getEntityId($this->result));

        return array("data" => $this->result, "curl" => $this);
    }


    public function getType(): string {
        return $this->type;
    }

    public function setType(string $type) {
        $this->type = $type;
        return $this;
    }

    public function getAuthorization(): string {
        return $this->authorization;
    }

    public function setAuthorization(string $authorization) {
        $this->authorization = $authorization;
        return $this;
    }

    public function getParameters() {
        return $this->parameters;
    }

    public function setParameters($parameters) {
        $this->parameters = $parameters;
        return $this;
    }

    public function getContentType(): string {
        return $this->contentType;
    }

    public function setContentType(string $contentType) {
        $this->contentType = $contentType;
        return $this;
    }
}