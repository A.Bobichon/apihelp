<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 09/12/18
 * Time: 14:03
 */

namespace App\Tests;

use App\Exception\NotFoundException;
use function PHPSTORM_META\elementType;

class MediaObject {

    public $id = null;
    public $file = null;
    public $isActive = null;
    public $contentUrl = null;
    public $rawProperties = null;

    public function __construct($file = "pictures/IMG_0695.JPG") {
        $this->file = $file;
        $this->isActive = "true";
        $this->owner = null;
        
    }

    // Add mediaObject to API
    public function create(User $user) {
        $file = null;
        if ( $this->file !== null || $this->file !== "") {
            $file = curl_file_create(realpath('./tests/medias/' . $this->file));
        }
        elseif ( $this->file === "" ){
            $file = "";
        }

        $lazyCurl = new LazyCurl("media_objects", $user->getToken(), array('file' => $file));
        $lazyCurl->setContentType('multipart/form-data');

        return $lazyCurl->execute($this);
    }

    public function update(User $user)
    {
        if($this->id !== null){
            $lazyCurl = new LazyCurl('media_objects/' . $this->id, $user->token);
            $lazyCurl->setType('PUT')->setParameters($this->rawProperties);
            $result = $lazyCurl->execute();
            return $result;
        }
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id; return $this;
    }

    public function getFile() {
        return $this->file;
    }

    public function setFile($file) {
        $this->file = $file; return $this;
    }

    public function setIsActive($active){
        $this->isActive = $active; return $this;
    }

    public function getIsActive(){
        return $this->isActive;
    }

    public function setContentUrl($contentUrl){
        $this->contentUrl = $contentUrl; return $this;
    }

    public function getContentUrl(){
        return $this->contentUrl;
    }

    public function getRawProperties(){
        return $this->rawProperties;
    }

    public function setRawProperties($rawProperties){
        $this->rawProperties = $rawProperties; return $this;
    }
}