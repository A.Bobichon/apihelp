<?php

namespace App\Tests;


class Library {

    public $id = null;
    /* @var $story Story */
    public $story = null;
    /* @var $membership Membership */
    public $membership = null;
    public $rawProperties = null;
    public $isActive = true;

    // Add Library to API
    public function create(User $user){
        $this->getStory() !== null ? $storyId = "api/stories/" . $this->getStory()->getId() : $storyId = null;
        $this->getMembership() !== null ? $membershipId = "api/memberships/" . $this->getMembership()->getId() : $membershipId = null;
        $parameters = sprintf('{"story": "%s", "membership": "%s"}',
            $storyId, $membershipId);

        $lazyCurl = new LazyCurl("libraries", $user->getToken(), $parameters);
        return $lazyCurl->execute($this);
    }

    //Update Library
    public function update(User $user){
        if($this->id !==null){
            $lazyCurl = new LazyCurl('libraries/' . $this->id, $user->token);
            $lazyCurl->setType('PUT')->setParameters($this->rawProperties);
            $result = $lazyCurl->execute();
            return $result;
        }
    }

    public function setId($id){
        $this->id = $id; return $this;
    }

    public function getId(){
        return $this->id;
    }

    public function getStory(){
        return $this->story;
    }

    public function setStory($story){
        $this->story = $story; return $this;
    }

    public function getMembership(){
        return $this->membership;
    }

    public function setMembership($membership){
        $this->membership = $membership; return $this;
    }

    public function getIsActive(){
        return $this->isActive;
    }

    public function setIsActive($isActive){
        $this->isActive = $isActive; return $this;
    }

    public function getRawProperties(){
        $this->rawProperties;
    }

    public function setRawProperties($rawProperties){
        $this->rawProperties = $rawProperties;
    }
}