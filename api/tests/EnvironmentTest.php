<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;

class EnvironmentTest extends TestCase {

    /* @var $user User */
    private $user = null;
    /* @var $membership Membership */
    private $membership = null;
    /* @var $picture MediaObject */
    private $picture = null;
    /* @var $sound MediaObject */
    private $sound = null;
    /* @var $account Account */
    private $account = null;
    /* @var $story Story */
    private $story = null;
    /* @var $library Library */
    private $library = null;
    /* @var $tag Tag */
    private $tag = null;

    public function setUp()
    {
        // SetUp user
        $this->user = new User();
        $this->user->create();
        $this->user->login();

        // SetUp picture
        $this->picture = new MediaObject();
        $this->picture->create($this->user);

        // SetUp sound
        $this->sound = new MediaObject();
        $this->sound->setFile("sounds/040D310AAD5781.mp3");
        $this->sound->create($this->user);

        // SetUp account
        $this->account = new Account();
        $this->account->setPicture($this->picture);
        $this->account->create($this->user);

        // SetUp membership
        $this->membership = new Membership();
        $this->membership->setAccount($this->account);
        $this->membership->create($this->user);

        // SetUp story
        $this->story = new Story();
        $this->story->setPicture($this->picture);
        $this->story->setSound($this->sound);
        $this->story->create($this->user);

        // SetUp tag
        $this->tag = new Tag();
        $this->tag->setOwner($this->user);
        $this->tag->setStory($this->story);
        $this->tag->create($this->user);

        // SetUp library
        $this->library = new Library();
        $this->library->setStory($this->story);
        $this->library->setMembership($this->membership);
        $this->library->create($this->user);

    }

    /*
    public function testSurcharge()
    {
        // Step1: Create User
        $admin = new User();
        $admin->create();
        $admin->login();

        // Step2: Create account with an admin
            //Set picture for account
        $result = $this->picture->create($admin);

        $this->assertEquals(201, $result["curl"]->infos["http_code"]);

        // Step3: Create account for Admin membership
        $account = $this->account;
        $account->setPicture($this->picture);
        $result = $this->account->create($admin);

        Utils::varDump($result, "ACCOUNT////");
        $this->assertEquals(201, $result["curl"]->infos["http_code"]);

        // Step4: Create MembershipAdmin
        $membership = $this->membership;
        $membership->setAccount($account);
        $result = $membership->create($admin);

        $this->assertEquals(201, $result["curl"]->infos["http_code"]);

        for ($i = 0; $i < 10; $i++) {
            // Create user for add membership
            $user = new User();
            $user->create();

            // Create membership whit the account
            $membership = new Membership();
            $membership->setAccount($account);
            $result = $membership->create($user, true);

            Utils::varDump($result, "MEMBERSHIP");

            $this->assertEquals(201, $result["curl"]->infos["http_code"]);

            $account = $this->account;
            $this->picture->create($user);
            $this->account->setPicture($this->picture);
            $this->account->create($user);
            $membership->setAccount($account);
            $result = $membership->create($user);
            $this->assertEquals(201, $result["curl"]->infos["http_code"]);
        }
    } */

    public function testSurchargeLibrary(){
        // Create Library
        $library = $this->library;

        // Create surcharge library wiht stories
        $story = $this->story;
        $story->setLibrary($library);
        $result = $story->create($this->user);

        for ($i=0; $i<100; $i++){
            $story = $this->story;
            $story->setLibrary($library);
            $result = $story->create($this->user);
            $this->assertEquals(201,$result['curl']->infos["http_code"]);
        }
        $this->assertEquals(201,$result['curl']->infos["http_code"]);


    }
}