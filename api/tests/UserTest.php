<?php

namespace App\Tests;

use League\Flysystem\Util;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    /* @var $user User */
    private $user = null;

    public function setUp()
    {
        // Iniatialize user
        $this->user = new User();
    }

    /**
     * Create user with good data
     */
    public function testNominal()
    {
        // Create User
        $result = $this->user->create();
        $this->assertEquals(201, $result["curl"]->infos["http_code"]);
    }

    /**
     * Connect user with good data
     */
    public function testConnection()
    {
        $this->user->create();
        $result = $this->user->login();
        $this->assertEquals(200, $result["curl"]->infos["http_code"]);
    }

    /**
     * firstname property
     */
    public function firstNameProperty($param)
    {
        // set the method
        $this->user->setFirstName($param);
        $result = $this->user->create();
        $this->assertEquals(400, $result["curl"]->infos['http_code']);
    }

    /** Test: firstName */
    public function testFirstNameProperty()
    {
        // Create user without firstname
        $this->firstNameProperty(null);
        // Create user with a too short firstname
        $this->firstNameProperty("1");
        // Create user with strind void
        $this->firstNameProperty("");
        // Create user with a too long firstname
        $this->firstNameProperty("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
    }

    /**
     * lastName property
     */
    public function lastNameProperty($params)
    {
        // set the method
        $this->user->setLastName($params);
        $result = $this->user->create();
        $this->assertEquals(400, $result["curl"]->infos['http_code']);
    }

    /** Test: lastname */
    public function testLastNameProperty()
    {
        // test with without lastName
        $this->lastNameProperty(null);
        // test with a too short lastName
        $this->lastNameProperty('1');
        // test with a too long lastName
        $this->lastNameProperty("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA aAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        // test with a void lastName
        $this->lastNameProperty("");
    }

    /**
     * username property
     */
    public function usernameProperty($param)
    {
        $this->user->setUsername($param);
        $result = $this->user->create();
        $this->assertEquals(400, $result["curl"]->infos['http_code']);
    }

    /** Test: username */
    public function testUsernameProperty()
    {
        // Create without username
        $this->usernameProperty(null);
        // Create username too long
        $this->usernameProperty("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        // Create username too short
        $this->usernameProperty("a@a");
        // Create username empty
        $this->usernameProperty("");
    }

    /**
     * password property
     */
    public function passwordProperty($params)
    {
        $this->user->setPassword($params);
        $result = $this->user->create();

        $this->assertEquals(400, $result["curl"]->infos["http_code"]);
    }

    /** Test: password  */
    public function testPasswordProperty()
    {
        // Create user with no password
        $this->passwordProperty(null);
        // Create user with password empty
        $this->passwordProperty("");
        // Create user with password too short
        $this->passwordProperty("1234");
        // Create user with password too long
        $this->passwordProperty("1234578910123457891012345789101234578910123457891012345789101234578910123457891012345789101234578910123457891012345789101234578910123457891012345789101234578910123457891012345789101234578910123457891012345789101234578910123457891012345789101234578910123456");
    }

    /**
     * Firstname property update
     */
    public function firstNamePropertyUpdate($params, $key = 400)
    {
        // Update Firstname user with good data
        $this->user->setFirstName($params);
        Utils::setUpdate($this->user, ['firstName']);
        $result = $this->user->update();
        $this->assertEquals($key, $result["curl"]->infos['http_code']);
    }

    /** Test: Firstname */
    public function testNameUpdateProperty()
    {
        // Create user and login
        $this->user->create();
        $this->user->login();
        // Update Firstname user with good data
        $this->firstNamePropertyUpdate("bertrand", 200);
        // log user with the new data
        $this->user->login();
        // Update user with no firstname
        $this->firstNamePropertyUpdate(null);
        // Update user with a firstname too long
        $this->firstNamePropertyUpdate("PlusieursoiresPlusieursoiresPlusieursoiresPlusieursoiresPlusieursoiresPlusieursoiresPlusieursoiresPlusieursoiresPlusieursoiresPlusieursoires");
        // Update user with void firstname
        $this->firstNamePropertyUpdate("");
    }

    /**
     * LastName property update
     */
    public function lastNamePropertyUpdate($params, $key = 400)
    {
        // Update with a good lastname
        $this->user->setLastName($params);
        Utils::setUpdate($this->user, ['lastName']);
        $result = $this->user->update();
        $this->assertEquals($key, $result["curl"]->infos['http_code']);
    }

    /** Test: Lastname */
    public function testLastNamePropertyUpdate()
    {
        // Create user and login
        $this->user->create();
        $this->user->login();
        // Update with a good lastname
        $this->lastNamePropertyUpdate("renard", 200);
        // log the user wiht new data
        $this->user->login();
        // Update with a null lastname
        $this->lastNamePropertyUpdate(null);
        // Update with a lastname
        $this->lastNamePropertyUpdate('1');
        // Update with a too long lastname
        $this->lastNamePropertyUpdate("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        // Update with a void lastname
        $this->lastNamePropertyUpdate("");
        // Update with a good lastname
        $this->lastNamePropertyUpdate("renard", 200);
    }

    /**
     * Username property update
     */
    public function usernamePropertyUpdate($params, $key = 400)
    {
        // Update with a good data username
        $this->user->setUsername($params);
        Utils::setUpdate($this->user, ['username']);
        $result = $this->user->update();
        $this->assertEquals($key, $result["curl"]->infos['http_code']);
    }

    /** Test: Username */
    public function testUsernameUpdateProperty()
    {
        // Create user and login
        $this->user->create();
        $this->user->login();
        // Update with a good data username
        $this->usernamePropertyUpdate(Utils::generateRandomValue() . "@mybookinou.com", 200);

        // log the user wiht new data
        $this->user->login();
        // Update without username
        $this->usernamePropertyUpdate(null);
        // Update with a wrong format username
        $this->usernamePropertyUpdate("WRONG");
        // Update with too long username
        $this->usernamePropertyUpdate('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
        // Update with a void username
        $this->usernamePropertyUpdate("");
    }

}