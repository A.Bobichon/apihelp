<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 08/12/18
 * Time: 22:48
 */

namespace App\Tests;

class Utils
{
    public static function getEntityId($result)
    {
        $path = json_decode($result)->{'@id'};
        $explode = explode("/", $path);
        return $explode[count($explode) - 1];
    }

    public static function loadUser(User & $user)
    {
        $lazyCurl = new LazyCurl("graphql", $user->getToken());
        $result = $lazyCurl->setContentType('application/graphql')->setParameters('{ users { edges { node { id username firstName lastName } } } }')->execute();

        // Spread user parameters
        $id = json_decode($result["data"])->data->users->edges[0]->node->id;
        $explode = explode("/", $id);
        $user->setId($explode[count($explode) - 1]);
        $username = json_decode($result["data"])->data->users->edges[0]->node->username;
        $user->setUsername($username);
        $firstName = json_decode($result["data"])->data->users->edges[0]->node->firstName;
        $user->setFirstName($firstName);
        $lastName = json_decode($result["data"])->data->users->edges[0]->node->lastName;
        $user->setLastName($lastName);

        return array("data" => $result, "curl" => $lazyCurl);
    }

    public static function setUpdate($entity, $listProperties)
    {

        $listProperty = get_object_vars($entity);
        $parameters = null;

        $parameters = '{';
        foreach ($listProperty as $key => $values) {
            if (in_array($key, $listProperties)) {
                if (is_object($values)) {
                    $values = serialize($values);
                }
                if (self::isBoolean($values)) {
                    $bool = ($values) ? 'true' : 'false';
                    $parameters .= sprintf('"' . $key . '": ' . $bool);
                } else if ($values !== null) {
                    $parameters .= sprintf('"' . $key . '": "' . $values . '"');
                }
            }
        }
        $parameters .= '}';
        $entity->setRawProperties($parameters);
    }

    ///////////////////////// A SUPPRIMER //////////////////////////////////
    public static function varDump($test, $name = "", $option = 1, $info = "data")
    {
        if ($option === 1) {
            echo "##### TEST " . $name . ":" . var_dump($test);
        } else {
            echo "##### TEST " . $name . "'' :", $test[$info];
        }
    }

    public static function isBoolean($var)
    {
        if (is_bool($var) || $var === "true" || $var === "false") return true;
        return false;
    }

    public static function generateRandomValue(){
        $chars = "azertyuiopqsdfghjklmwxcvbn";
        $string = "";

        for($i = 0; $i < 6; $i++){
           $string .= $chars[rand(0, strlen($chars)-1)];
        }
        return $string;
    }
}