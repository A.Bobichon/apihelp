<?php

namespace App\Tests;

use League\Flysystem\Util;
use PHPUnit\Framework\TestCase;

class TagTest extends TestCase {
    /* @var $user User */
    private $user = null;
    /* @var $tag Tag */
    private $tag = null;
    /* @var $story Story */
    private $story = null;

    public function setUp(){
        // Step 1: Create user
        $this->user = new User();
        $owner = $this->user;
        $owner->create();
        $owner->login();

        // Step 2: Create media
        $picture = new MediaObject();
        $picture->create($owner);

        $sound = new MediaObject();
        $sound->setFile("sounds/04E4CA0A542880.mp3");
        $sound->create($owner);

        // Step 3: Create story
        $story = $this->story = new Story();
        $story->setPicture($picture);
        $story->setSound($sound);
        $story->create($owner);

        // Step 4: Create tag
        $this->tag = new Tag();
        $this->tag->setStory($story);
        $this->tag->setOwner($owner);
    }

    /**
     * Create tag with good data
     */
    public function testNominal(){
        $result = $this->tag->create($this->user);
        $this->assertEquals(201, $result["curl"]->infos["http_code"]);
    }

    /**
     * Token property
     */
    public function tokenProperty($params) {
        // Create tag without token
        $this->user->setToken($params);
        $result = $this->tag->create($this->user);
        $this->assertEquals(401, $result["curl"]->infos['http_code']);
    }

    /** Test: token */
    public function testTokenProperty() {
        // Create tag without token
        $this->tokenProperty(null);
        // Create tag with a void token
        $this->tokenProperty("");
        // Create tag with bad token
        $this->tokenProperty("eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1NDQxMDQxODcsImV4cCI6MTU0NDEwNzc4Nywicm9sZXMiOlsiUk9MRV9VU0VSX0FQUCJdLCJ1c2VybmFtZSI6Im5vcmJlcnRAbXlib29raW5vdS5jb20ifQ.phkdEa0eirz6hQ0SyVb5hifSStemDfT8YLE7KY7joudOp85vgItlFBAtTWdOMTPanJ_yckJcR6yj2M7LEgafyun_jNW94ju3V0V903drd156oVZUYPvFmvI1uwokGTYSyhruSANpsf64_axW6RwfUfom05-adSuowyrVZqWOyPT-s79QPn3botOZkLSpzTTtb0pgtqLOAuKUkUXyUI9ib_qxNlQw0A4fPf12hN49Cemq83MQS6H1wu9ksO2__sptoL5ll_3khjRVlFTc9p5vSYWRR-BYekEiIj9Mwv58LkiEWj1rHbE2NLl6uVTTHk10JRroSay3gL8XHTG8LQ5rJHSBpnIMBM3efeayAA8ulOCYcf9N4R6Z_vnFBNt5dowE9i4zbm8LK--6O_ERmeBeaSBNOXfzU-9Xug7eTEpJpfBIr1p6fkXOpOMWNwesJkliuyT5kcYmOcwG229rCGnEIPKB_6L8aacbSVM6oZYM32NggNzrB0vbMwcs14jMHCS5oIy4x3GBp-Qe5FrYrT8sCgr040y-c83N99zTwYO2tbSIFpVqruUCgMi0pS5QpqeGkMkBiqDDQJPV1mEzpuUgaIX7TYiSiiix-n1_V73nKJ2t8sgg5hB1wjbK1QLCLU52MOFoI7ynTZO1RYVoguXHAhojM-thIX7D32Zyfy4ZthA");
    }

    /**
     * UiDevice property
     */
    public function uiDeviceProperty($params){
        $this->tag->setUid($params);
        $result = $this->tag->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos["http_code"]);
    }

    /** Test: uiDevice */
    public function testUiDeviceProperty(){
        // test whit a empty uiDevice
        $this->uiDeviceProperty("");
        // test whit a uiDevice null
        $this->uiDeviceProperty(null);
        // test whit a to short property
        $this->uiDeviceProperty("1");
        // test whit a to long property
        $this->uiDeviceProperty("12345678910123456789101234567891012345678910123456789101234567891012345678910123456789101234567891012345678910");
    }

    /**
     * Story property
     */
    public function storyProperty($params){
        // Create a tag without Story
        $this->tag->setStory($params);
        $result = $this->tag->create($this->user);
        $this->assertEquals(400, $result["curl"]->infos['http_code']);
    }

    /** Test: story */
    public function testStoryProperty(){
        // Create with wrong story user
        $this->story->setId("999");
        $this->storyProperty($this->story);
        // Create a tag without Story
        $this->storyProperty(null);
        // Create a tag with a void story
        $this->story->setId("");
        $this->storyProperty($this->story);
        // Create tag whit a wrong story format
        $this->story->setId("WRONG");
        $this->storyProperty($this->story);
    }

    /**
     * Owner property
     */
    public function ownerProperty($params){
        // Create tag with wrong user
        $this->tag->setOwner($params);
        $result = $this->tag->create($this->user);
        $this->assertEquals(201, $result["curl"]->infos['http_code']);
    }

    /** Test: owner */
    public function testOwnerProperty(){
        // Create tag without user
        $this->ownerProperty($this->tag->setOwner(null));
        // Create tag with void user
        $this->ownerProperty($this->tag->setOwner("api/users/" . $this->user->setId("999999")->getId()));
        // Create tag with a wrong user format
        $this->ownerProperty($this->tag->setOwner("api/users/"));
        // Create tag with wrong user
        $this->ownerProperty($this->tag->setOwner("api/users/3243243243243242332432"));
    }

    /**
     * UiDevice propertyUpdate
     */
    public function uiDevicePropertyUpdate($params, $key = 400){
        $this->tag->setUid($params);
        Utils::setUpdate($this->tag, ["uid"]);
        $result = $this->tag->update($this->user);
        $this->assertEquals($key, $result["curl"]->infos['http_code']);
    }

    /** Test: uiDevice */
    public function testUiDevicePropertyUpdate(){
        // Create tag for test
        $this->tag->create($this->user);
        // update whit a good uiDevice whit a good data
        $this->uiDevicePropertyUpdate("Comique", 200);
        // update whit a empty uiDevice
        $this->uiDevicePropertyUpdate("");
        // update whit a uiDevice null
        $this->uiDevicePropertyUpdate(null);
        // update whit a to short property
        $this->uiDevicePropertyUpdate("1");
        // update whit a to long property
        $this->uiDevicePropertyUpdate("12345678910123456789101234567891012345678910123456789101234567891012345678910123456789101234567891012345678910");
    }

    /**
     * Story propertyUpdate
     */
    public function storyPropertyUpdate($params, $key = 400){
        $this->tag->setStory($params);
        Utils::setUpdate($this->tag, ["story"]);
        $result = $this->tag->update($this->user);
        $this->assertEquals($key, $result["curl"]->infos['http_code']);
    }

    /** Test: story */
    public function testStoryPropertyUpdate(){
        $tag = $this->tag;
        $tag->create($this->user);

        // Update with a good story
        $this->storyPropertyUpdate("api/stories/" . $tag->story->getId(), 200);
        // Update  with no story
        $this->storyPropertyUpdate(null);
        //Update with wrong story
        $this->story->setId("99999");
        $this->storyPropertyUpdate($this->story);
        //Update with wrong story format
        $this->story->setId("blablabla");
        $this->storyPropertyUpdate($this->story);
    }

   /**
    * Test Update environment
    */
   public function testOwnerUpdatesOtherTag()
   {
       // Set up the user 1
       $user1 = $this->user;

       //  test if user2 can change user1f
       $this->tag->create($user1);

       // Create user2
       $user2 = new User();
       $user2->create();
       $this->tag->setIsActive(false);
       Utils::setUpdate($this->tag, ["isActive"]);
       $result = $this->tag->update($user2);
       $this->assertEquals(401, $result["curl"]->infos['http_code']);

       // use User1 for change the tag
       $this->tag->setIsActive(false);
       Utils::setUpdate($this->tag, ["isActive"]);
       $result = $this->tag->update($user1);
       $this->assertEquals(200, $result["curl"]->infos['http_code']);

       // try to reactive the tag
       $this->tag->setIsActive(true);
       Utils::setUpdate($this->tag, ["isActive"]);
       $result = $this->tag->update($user1);
       $this->assertEquals(404, $result["curl"]->infos['http_code']);
   }
}

